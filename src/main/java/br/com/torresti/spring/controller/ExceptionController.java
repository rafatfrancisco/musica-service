package br.com.torresti.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.torresti.spring.to.RestTO;
import br.com.torresti.spring.util.SituacaoRest;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Throwable.class)
    public RestTO tratarExcecaoGenerica(final HttpServletRequest req, final Throwable ex) throws Throwable {
    	return new RestTO(SituacaoRest.Erro.getCodigo(), ex.getMessage());
    }
	
}
