package br.com.torresti.musica.service.component;

import org.springframework.stereotype.Component;

import br.com.torresti.musica.service.util.ClienteRest;
import br.com.torresti.musica.util.to.AlbumTO;

@Component
public class LojaComponent {
	
	public AlbumTO consultarId(Integer tipo, String id) {
		return new ClienteRest<AlbumTO>().get("http://localhost:8080/disco-service/loja/consultar/id/{tipo}/{id}", AlbumTO.class, tipo, id);
	}

}
