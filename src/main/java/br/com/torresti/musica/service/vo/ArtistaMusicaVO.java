package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.musica.crud.model.entidy.TagArtistaMusica;

public class ArtistaMusicaVO {

	private Integer idArtistaMusica;

	private String musica;

	private String artistaOriginal;

	private String musicaOriginal;

	private String queryPesquisa;

	private String pesquisaMusica;

	private String link;

	private Integer categoria;

	private Integer situacao;

	private List<TagArtistaMusica> listaTagArtistaMusica;

	public Integer getIdArtistaMusica() {
		return idArtistaMusica;
	}

	public void setIdArtistaMusica(Integer idArtistaMusica) {
		this.idArtistaMusica = idArtistaMusica;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public String getArtistaOriginal() {
		return artistaOriginal;
	}

	public void setArtistaOriginal(String artistaOriginal) {
		this.artistaOriginal = artistaOriginal;
	}

	public String getMusicaOriginal() {
		return musicaOriginal;
	}

	public void setMusicaOriginal(String musicaOriginal) {
		this.musicaOriginal = musicaOriginal;
	}

	public String getQueryPesquisa() {
		return queryPesquisa;
	}

	public void setQueryPesquisa(String queryPesquisa) {
		this.queryPesquisa = queryPesquisa;
	}

	public String getPesquisaMusica() {
		return pesquisaMusica;
	}

	public void setPesquisaMusica(String pesquisaMusica) {
		this.pesquisaMusica = pesquisaMusica;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public List<TagArtistaMusica> getListaTagArtistaMusica() {
		return listaTagArtistaMusica;
	}

	public void setListaTagArtistaMusica(List<TagArtistaMusica> listaTagArtistaMusica) {
		this.listaTagArtistaMusica = listaTagArtistaMusica;
	}

}
