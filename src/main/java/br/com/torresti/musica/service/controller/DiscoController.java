package br.com.torresti.musica.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torresti.musica.service.service.DiscoService;
import br.com.torresti.musica.util.to.CompraDiscoTO;
import br.com.torresti.spring.to.RestTO;

@RestController
@RequestMapping(value = "/disco-service")
public class DiscoController {

	@Autowired
	private DiscoService discoService;

	@PostMapping(value = "/compra-disco/salvar")
	public ResponseEntity<RestTO> salvarCompraDisco(@RequestBody CompraDiscoTO compraDiscoTO) {
		return new ResponseEntity<RestTO>(discoService.salvarCompraDisco(compraDiscoTO), HttpStatus.OK);
	}
	
	@GetMapping(value = "/compra-disco/remover/{idDisco}")
	public ResponseEntity<RestTO> removerCompraDisco(@PathVariable Integer idDisco) {
		return new ResponseEntity<RestTO>(discoService.remover(idDisco), HttpStatus.OK);
	}

}
