package br.com.torresti.musica.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.service.component.DiscoComponent;
import br.com.torresti.musica.util.to.CompraDiscoTO;
import br.com.torresti.spring.to.RestTO;

@Component
public class DiscoService {
	
	@Autowired
	private DiscoComponent discoComponent;
	
	/*
	public PesquisaAlbumTO consultarGeral(String query) {
		
		PesquisaAlbumTO pesquisaAlbumTO = new PesquisaAlbumTO();
		
		try {
			
			AlbumTO junoAlbumTO = junoComponent.consultarQuery(query);
			
			pesquisaAlbumTO.setJunoAlbumTO(junoAlbumTO);
			
			AlbumTO discogsAlbumTO = discogsComponent.consultar(query);
			
			pesquisaAlbumTO.setDiscogsAlbumTO(discogsAlbumTO);
			
			AlbumTO beatportAlbumTO = beatportComponent.consultar(query);
			
			pesquisaAlbumTO.setBeatportAlbumTO(beatportAlbumTO);

			AlbumTO decksAlbumTO = consultarDecks(query);
			
			pesquisaAlbumTO.setDecksAlbumTO(decksAlbumTO);
			
		} catch (Exception e) {
			
			pesquisaAlbumTO.setCodigo(SituacaoRest.Erro.getCodigo());
			
			pesquisaAlbumTO.setMensagem(e.getMessage());
			
		}
		
		return pesquisaAlbumTO;
		
	}
	
	public AlbumTO consultarDecks(String query) {
		
		return decksComponent.consultarQuery(query);
			
	}
	*/
	
	public RestTO salvarCompraDisco(CompraDiscoTO compraDiscoTO) {

		try {
			
			discoComponent.salvarCompraDisco(compraDiscoTO);
			
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			return RestTO.erro(e);
			

		}

	}
	
	/*
	public PesquisaDiscoDTO pesquisarDiscoCase(PesquisaDiscoDTO pesquisaDiscoDTO) {

		try {
						
			discoComponent.pesquisarDiscoCase(pesquisaDiscoDTO);
			
			return pesquisaDiscoDTO;
			
		} catch (Exception e) {
			
			return new PesquisaDiscoDTO(RestTO.erro(e));

		}

	}
	
	public RestTO processarImagem(Integer idDisco, Integer quantidade) {
		
		try {
			
			discoComponent.processarImagem(idDisco, quantidade);
						
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			return RestTO.erro(e);

		}
		
	}
	
	public RestTO ajustarLinkErradoImagem() {
		
		try {
			
			discoComponent.ajustarLinkErradoImagem();
						
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			return RestTO.erro(e);

		}
		
	}
	
	public RestTO ajustarRecadastro(Integer idDisco) {
		
		try {
			
			discoComponent.ajustarRecadastro(idDisco);
						
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			return RestTO.erro(e);

		}
		
	}	
	*/

	public RestTO remover(Integer idDisco) {
		
		try {
			
			discoComponent.excluir(idDisco);
						
			return RestTO.sucesso();
			
		} catch (Exception e) {
			
			return RestTO.erro(e);

		}
		
	}
	
}
