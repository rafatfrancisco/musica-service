package br.com.torresti.musica.service.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.torresti.musica.service.component.DiscoViewComponent;
import br.com.torresti.musica.service.vo.AlteraMusicaDiscoVO;
import br.com.torresti.musica.service.vo.ConsultaDiscoVO;
import br.com.torresti.musica.service.vo.OrganizaDiscoVO;
import br.com.torresti.spring.to.AjaxTO;

@Controller
@RequestMapping(value = "/disco-view")
public class DiscoView {

	@Autowired
	private DiscoViewComponent discoViewComponent;
	
	@RequestMapping(value = "/case-disco/consulta/carregar", method = RequestMethod.GET)
	public ModelAndView carregarTelaConsultaDisco() {
		return new ModelAndView("jsp/disco/consulta", "consultaDiscoVO", discoViewComponent.carregarTelaConsultaDisco());
	}
	
	@RequestMapping(value = "/case-disco/consulta/consultar", method = RequestMethod.POST)
	public ModelAndView consultarDisco(@ModelAttribute("consultaDiscoVO") ConsultaDiscoVO consultaDiscoVO) {
		return new ModelAndView("jsp/disco/consulta", "consultaDiscoVO", discoViewComponent.consultarDisco(consultaDiscoVO));
	}
		
	@RequestMapping(value = "/case-disco/organiza/carregar", method = RequestMethod.GET)
	public ModelAndView carregarTelaOrganizaDisco() {
		return new ModelAndView("jsp/disco/organiza", "organizaDiscoVO", discoViewComponent.carregarTelaOrganizaDisco());
	}
	
	@RequestMapping(value = "/case-disco/organiza/consultar", method = RequestMethod.POST)
	public ModelAndView consultarOrganizaDisco(@ModelAttribute("organizaDiscoVO") OrganizaDiscoVO organizaDiscoVO) {
		return new ModelAndView("jsp/disco/organiza", "organizaDiscoVO", discoViewComponent.consultarOrganizaDisco(organizaDiscoVO));
	}
	
	@ResponseBody
	@RequestMapping(value = "/case-disco/organiza/ajustar/ajax", method = RequestMethod.POST)
	public AjaxTO<Integer> ajustarDiscoAjax(@ModelAttribute("organizaDiscoVO") OrganizaDiscoVO organizaDiscoVO) {
		try {
			discoViewComponent.ajustarOrganizaDisco(organizaDiscoVO);
			return AjaxTO.sucesso("Discos organizados!");
		} catch (Exception e) {
			return AjaxTO.erro("Erro ao ajustar discos. " + e.getMessage());
		}
	}
	
	@RequestMapping(value = "/case-disco/consulta/musicas/{idDisco}", method = RequestMethod.GET)
	public ModelAndView carregarTelaAlteraMusicaDisco(@PathVariable("idDisco") Integer idDisco) {
		return new ModelAndView("jsp/disco/consulta", "alteraMusicaDiscoVO", discoViewComponent.carregarTelaAlteraMusicaDisco(idDisco));
	}
	
	@RequestMapping(value = "/case-disco/salvar/musicas/ajax", method = RequestMethod.POST)
	public AjaxTO<Integer> salvarMusicasAjax(@ModelAttribute("alteraMusicaDiscoVO") AlteraMusicaDiscoVO alteraMusicaDiscoVO) {
		try {
			discoViewComponent.salvarMusicaDisco(alteraMusicaDiscoVO);
			return AjaxTO.sucesso();
		} catch (Exception e) {
			return AjaxTO.erro("Erro ao salvar musicas " + e.getMessage());
		}
	}
	
}
