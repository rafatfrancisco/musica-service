package br.com.torresti.musica.service.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Disco;
import br.com.torresti.musica.crud.model.entidy.Prateleira;
import br.com.torresti.musica.util.StringUtil;

@Repository
public class DiscoRepository extends BaseRepository<Disco, Integer> {

	@SuppressWarnings("unchecked")
	public List<Disco> listar() {

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d ");
		sql.append("FROM Disco d ");
		Query query = entityManager.createQuery(sql.toString());
		
		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultar(String id, String codigo, String selo, String artista, String musica, String nome) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT d ");
		sql.append("FROM Disco d ");
		if (!StringUtil.isEmpty(artista) || !StringUtil.isEmpty(musica)) sql.append(", MusicaDisco m ");
		sql.append("WHERE d.situacao = :situacao ");
		if (!StringUtil.isEmpty(id)) {
			sql.append("AND d.idDisco = :id ");
		} else if (!StringUtil.isEmpty(codigo)) {
			sql.append("AND d.urlSite like :urlSite ");
		} else {
			if (!StringUtil.isEmpty(selo)) sql.append("AND d.selo like :selo ");
			if (!StringUtil.isEmpty(nome)) sql.append("AND d.nome like :nome ");
			if (!StringUtil.isEmpty(artista) || !StringUtil.isEmpty(musica)) sql.append("AND m.disco.idDisco = d.idDisco ");
			if (!StringUtil.isEmpty(artista)) sql.append("AND m.artista like :artista ");
			if (!StringUtil.isEmpty(musica)) sql.append("AND m.musica like :musica ");
		}
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("situacao",0);
		if (!StringUtil.isEmpty(id)) {
			query.setParameter("id",Integer.parseInt(id));
		} else if (!StringUtil.isEmpty(codigo)) {
			query.setParameter("urlSite", "%" + codigo + "%");
		} else {
			if (!StringUtil.isEmpty(selo)) query.setParameter("selo", "%" + selo + "%");
			if (!StringUtil.isEmpty(nome)) query.setParameter("nome", "%" + nome + "%");
			if (!StringUtil.isEmpty(artista)) query.setParameter("artista","%" + artista + "%");
			if (!StringUtil.isEmpty(musica)) query.setParameter("musica","%" + musica + "%");
		}
		
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<Integer> consultarMenor(Integer idDisco) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d.idDisco ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.idDisco < :idDisco ");
		sql.append("ORDER BY d.idDisco ");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idDisco",idDisco);
		
		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultarMenor(Integer idDisco, Prateleira prateleira, Integer quantidade) {	
		
		StringBuffer sql = new StringBuffer();
		
		if (prateleira != null) {
		
			sql.append("SELECT d ");
			sql.append("FROM Disco d, Prateleira p ");
			sql.append("WHERE p.idPrateleira = :idPrateleira ");
			sql.append("AND d.prateleira = p ");
			sql.append("AND d.idDisco < :idDisco ");
			sql.append("ORDER BY d.idDisco DESC ");
			
			Query query = entityManager.createQuery(sql.toString());
			query.setParameter("idPrateleira", prateleira.getIdPrateleira());
			query.setParameter("idDisco",idDisco);
			query.setMaxResults(quantidade);
			
			return query.getResultList();
		
		} else {
			
			sql.append("SELECT d ");
			sql.append("FROM Disco d ");
			sql.append("WHERE d.idDisco < :idDisco ");
			sql.append("ORDER BY d.idDisco DESC ");
			
			Query query = entityManager.createQuery(sql.toString());
			query.setParameter("idDisco",idDisco);
			query.setMaxResults(quantidade);
			
			return query.getResultList();
			
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultarMaior(Integer idDisco, Prateleira prateleira, Integer quantidade) {	
		
		StringBuffer sql = new StringBuffer();
		
		if (prateleira != null) {
			
			sql.append("SELECT d ");
			sql.append("FROM Disco d, Prateleira p ");
			sql.append("WHERE p.idPrateleira = :idPrateleira ");
			sql.append("AND d.prateleira = p ");
			sql.append("AND d.idDisco > :idDisco ");
			sql.append("ORDER BY d.idDisco ");
			
			Query query = entityManager.createQuery(sql.toString());
			query.setParameter("idPrateleira",prateleira.getIdPrateleira());
			query.setParameter("idDisco",idDisco);
			query.setMaxResults(quantidade);
			
			return query.getResultList();
			
		} else {
			
			sql.append("SELECT d ");
			sql.append("FROM Disco d ");
			sql.append("WHERE d.idDisco > :idDisco ");
			sql.append("ORDER BY d.idDisco ");
			
			Query query = entityManager.createQuery(sql.toString());
			query.setParameter("idDisco",idDisco);
			query.setMaxResults(quantidade);
			
			return query.getResultList();
			
		}

	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultarMaiorSemImagem(Integer idDisco, Integer quantidade) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.idDisco > :idDisco ");
		sql.append("AND d.urlImagem IS NULL ");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idDisco",idDisco);
		query.setMaxResults(quantidade);
		
		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> consultarDiscoPrateleira(Integer idPrateleira) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d.idDisco ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.idPrateleira = :idPrateleira ");
		
		Query query = entityManager.createNativeQuery(sql.toString());
		query.setParameter("idPrateleira",idPrateleira);
 		
		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultarTempAjustarImagem(Integer tipo, String urlImagem) {	
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.tipo = :tipo ");
		sql.append("AND d.urlImagem like :urlImagem ");
		sql.append("ORDER BY d.idDisco ");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("tipo", tipo);
		query.setParameter("urlImagem", urlImagem + "%");
		
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<Disco> consultarAjustarOrdemDeletar(Integer idPrateleira) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.prateleira.idPrateleira = :idPrateleira ");
		sql.append("AND d.situacao <> 0 ");
		sql.append("ORDER BY d.idDisco ");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idPrateleira", idPrateleira);
		
		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<Disco> consultarTempAjustarOrdem(Integer idPrateleira) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d ");
		sql.append("FROM Disco d ");
		sql.append("WHERE d.prateleira.idPrateleira = :idPrateleira ");
		sql.append("AND d.situacao = 0 ");
		sql.append("ORDER BY d.idDisco ");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idPrateleira", idPrateleira);
		
		return query.getResultList();

	}
	
}
