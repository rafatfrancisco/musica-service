package br.com.torresti.musica.service.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.service.component.AlbumComponent;
import br.com.torresti.musica.util.dto.CadastroDescksDTO;
import br.com.torresti.musica.util.dto.CadastroDownloadDTO;
import br.com.torresti.spring.to.RestTO;

@Component
public class DownloadService {
	
	@Autowired
	private AlbumComponent albumComponent;
	
	public List<RestTO> cadastrarDecks(CadastroDescksDTO cadastroDescksDTO) {

		List<RestTO> retorno = new ArrayList<>();
			
		for (String urlDecks : cadastroDescksDTO.getListaUrlDecks()) {
			
			try {
				
				albumComponent.salvar(2, urlDecks);
				
				retorno.add(RestTO.sucesso("Sucesso ao processar urlDecks " + urlDecks));
				
			} catch (Exception e) {
				
				retorno.add(RestTO.erro("Erro ao processar urlDecks " + urlDecks + " - "+ e.getMessage()));
				
			}
			
		}
		
		return retorno;
		
	}
	
	public List<RestTO> cadastrarDownload(CadastroDownloadDTO cadastroDownloadDTO) {

		List<RestTO> retorno = new ArrayList<>();
			
		for (String url : cadastroDownloadDTO.getListaUrl()) {
			
			try {
				
				albumComponent.salvar(cadastroDownloadDTO.getTipo(), url);
				
				retorno.add(RestTO.sucesso("Sucesso ao processar urlDecks " + url));
				
			} catch (Exception e) {
				
				retorno.add(RestTO.erro("Erro ao processar urlDecks " + url + " - "+ e.getMessage()));
				
			}
			
		}
		
		return retorno;
		
	}

}
