package br.com.torresti.musica.service.vo;

public class MusicaDiscoVO {

	private Integer idMusicaDisco;
	
	private String artista;
	
	private String musica;
	
	public MusicaDiscoVO() {}
	
	public MusicaDiscoVO(Integer idMusicaDisco, String artista, String musica) {
		this.idMusicaDisco = idMusicaDisco;
		this.artista = artista;
		this.musica = musica;
	}

	public Integer getIdMusicaDisco() {
		return idMusicaDisco;
	}

	public void setIdMusicaDisco(Integer idMusicaDisco) {
		this.idMusicaDisco = idMusicaDisco;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}
	
	
	
}
