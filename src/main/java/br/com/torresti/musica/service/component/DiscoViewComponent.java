package br.com.torresti.musica.service.component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.musica.service.vo.AlteraMusicaDiscoVO;
import br.com.torresti.musica.service.vo.ConsultaDiscoVO;
import br.com.torresti.musica.service.vo.MusicaDiscoVO;
import br.com.torresti.musica.service.vo.OrganizaDiscoVO;
import br.com.torresti.musica.util.dto.DiscoDTO;
import br.com.torresti.musica.util.dto.ResultadoDiscoDTO;

@Component
public class DiscoViewComponent {

	@Autowired
	private DiscoComponent discoComponent;
	
	public ConsultaDiscoVO carregarTelaConsultaDisco() {
		return new ConsultaDiscoVO(MenuTela.DiscoCaseConsulta);		
	}

	public void carregarConsultarDisco(ConsultaDiscoVO consultaDiscoVO) {
		consultaDiscoVO.recarregarTitulo(MenuTela.DiscoCaseConsulta);
		consultaDiscoVO.setListaDiscoDTO(new ArrayList<DiscoDTO>());
		consultaDiscoVO.setListaResultadoDiscoDTO(new ArrayList<ResultadoDiscoDTO>());
		consultaDiscoVO.setListaPrateleira(discoComponent.listarPrateleira());
	}
		
	public ConsultaDiscoVO consultarDisco(ConsultaDiscoVO consultaDiscoVO) {
		
		this.carregarConsultarDisco(consultaDiscoVO);
				
		List<DiscoDTO> listaDiscoDTO = discoComponent.pesquisarDiscoCase(consultaDiscoVO.getId(), consultaDiscoVO.getCodigo(), consultaDiscoVO.getSelo(), consultaDiscoVO.getArtista(), consultaDiscoVO.getMusica(), consultaDiscoVO.getAlbum());
		
		if (listaDiscoDTO != null && !listaDiscoDTO.isEmpty()) {
			if (listaDiscoDTO.size() > 1) {
				consultaDiscoVO.setListaDiscoDTO(listaDiscoDTO);
			} else {
				consultaDiscoVO.setListaResultadoDiscoDTO(discoComponent.pesquisarDiscoCase(listaDiscoDTO.get(0).getIdDisco()));
			}
		}
		
		/*
		PesquisaDiscoDTO pesquisaDiscoDTO = new PesquisaDiscoDTO();
		pesquisaDiscoDTO.setQuantidade(10);
		BeanUtils.copyProperties(consultaDiscoVO, pesquisaDiscoDTO);
		
		pesquisaDiscoDTO = discoComponent.pesquisarDiscoCase(pesquisaDiscoDTO);
		consultaDiscoVO.setListaResultadoDiscoDTO(pesquisaDiscoDTO.getListaResultadoDiscoDTO());
		*/
		
		return consultaDiscoVO;
		
	}
	
	public OrganizaDiscoVO carregarTelaOrganizaDisco() {
		OrganizaDiscoVO organizaDiscoVO = new OrganizaDiscoVO();
		this.carregarOrganizaDisco(organizaDiscoVO);
		return organizaDiscoVO;	
	}
	
	public void carregarOrganizaDisco(OrganizaDiscoVO organizaDiscoVO) {
		organizaDiscoVO.recarregarTitulo(MenuTela.DiscoCaseOrganiza);
		organizaDiscoVO.setListaPrateleira(discoComponent.listarPrateleira());
	}

	public OrganizaDiscoVO consultarOrganizaDisco(OrganizaDiscoVO organizaDiscoVO) {
		
		this.carregarOrganizaDisco(organizaDiscoVO);
		
		List<DiscoDTO> listaDiscoDTO = discoComponent.consultarOrganizaDisco(organizaDiscoVO.getIdDisco());
		for (DiscoDTO discoDTO : listaDiscoDTO) {
			if (organizaDiscoVO.getIdPrateleira() != null) discoDTO.setIdPrateleira(organizaDiscoVO.getIdPrateleira());
		}
		
		organizaDiscoVO.setIdPrateleira(null);
		
		organizaDiscoVO.setListaDiscoDTO(listaDiscoDTO);
		
		return organizaDiscoVO;
		
	}
		
	public void ajustarOrganizaDisco(OrganizaDiscoVO organizaDiscoVO) {
		discoComponent.ajustarDisco(organizaDiscoVO.getListaDiscoDTO());				
	}

	// - ALTERAR MUSICAS - //
	
	public AlteraMusicaDiscoVO carregarTelaAlteraMusicaDisco(Integer idDisco) {
		AlteraMusicaDiscoVO alteraMusicaDiscoVO = new AlteraMusicaDiscoVO();
		alteraMusicaDiscoVO.setListaMusicaDiscoVO(discoComponent.obterDisco(idDisco).getListaDiscoMusica().stream().map(e -> new MusicaDiscoVO(e.getIdMusicaDisco(), e.getArtista(), e.getMusica())).collect(Collectors.toList()));
		return alteraMusicaDiscoVO;		
	}

	public void salvarMusicaDisco(AlteraMusicaDiscoVO alteraMusicaDiscoVO) {
		discoComponent.salvarMusicaDisco(alteraMusicaDiscoVO.getListaMusicaDiscoVO());
	}
	
}
