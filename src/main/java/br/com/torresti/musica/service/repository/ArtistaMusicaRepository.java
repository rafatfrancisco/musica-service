package br.com.torresti.musica.service.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.ArtistaMusica;

@Repository
public class ArtistaMusicaRepository extends BaseRepository<ArtistaMusica, Integer> {
	
	@SuppressWarnings("unchecked")
	public List<ArtistaMusica> listar(Integer idAlbum) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p FROM ArtistaMusica p WHERE p.album.idAlbum = :idAlbum");
		
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idAlbum",idAlbum);

		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<ArtistaMusica> listar(Integer situacao, Integer categoria) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p ");
		sql.append("FROM ArtistaMusica p ");
		sql.append("WHERE p.situacao = :situacao ");
		if (categoria != null)	sql.append("AND p.categoria = :categoria ");
					
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("situacao",situacao);
		if (categoria != null)	query.setParameter("categoria",categoria);

		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public List<ArtistaMusica> listar(Integer idTag, Integer situacao, Integer categoria) {	
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p ");
		sql.append("FROM Tag t, TagArtistaMusica x, ArtistaMusica p ");
		sql.append("WHERE t.idTag = :idTag ");
		sql.append("AND x.tag = t ");
		sql.append("AND p = x.artistaMusica ");
		sql.append("AND p.situacao = :situacao ");
		if (categoria != null)	sql.append("AND p.categoria = :categoria ");
					
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("idTag",idTag);
		query.setParameter("situacao",situacao);
		if (categoria != null)	query.setParameter("categoria",categoria);

		return query.getResultList();

	}

}
