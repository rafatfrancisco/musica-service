package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.musica.util.dto.DiscoDTO;
import br.com.torresti.musica.util.dto.ResultadoDiscoDTO;
import br.com.torresti.spring.to.BaseTO;
import br.com.torresti.spring.to.CodigoDescricaoTO;

public class ConsultaDiscoVO extends BaseTO {

	private String id;

	private String codigo;

	private String selo;

	private String artista;

	private String musica;

	private String album;

	private List<CodigoDescricaoTO<Integer>> listaPrateleira;

	private List<DiscoDTO> listaDiscoDTO;

	private List<ResultadoDiscoDTO> listaResultadoDiscoDTO;

	public ConsultaDiscoVO() {
	}

	public ConsultaDiscoVO(MenuTela menuTela) {
		super(menuTela.getTitulo(), menuTela.getCaminho());
	}

	public void recarregarTitulo(MenuTela menuTela) {
		this.setTitulo(menuTela.getTitulo());
		this.setCaminho(menuTela.getCaminho());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getSelo() {
		return selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public List<CodigoDescricaoTO<Integer>> getListaPrateleira() {
		return listaPrateleira;
	}

	public void setListaPrateleira(List<CodigoDescricaoTO<Integer>> listaPrateleira) {
		this.listaPrateleira = listaPrateleira;
	}

	public List<DiscoDTO> getListaDiscoDTO() {
		return listaDiscoDTO;
	}

	public void setListaDiscoDTO(List<DiscoDTO> listaDiscoDTO) {
		this.listaDiscoDTO = listaDiscoDTO;
	}

	public List<ResultadoDiscoDTO> getListaResultadoDiscoDTO() {
		return listaResultadoDiscoDTO;
	}

	public void setListaResultadoDiscoDTO(List<ResultadoDiscoDTO> listaResultadoDiscoDTO) {
		this.listaResultadoDiscoDTO = listaResultadoDiscoDTO;
	}

}
