package br.com.torresti.musica.service.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.crud.model.entidy.Album;
import br.com.torresti.musica.crud.model.entidy.ArtistaMusica;
import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.musica.service.util.MusicaConstantes.TagDiscoArtistaMusica;
import br.com.torresti.musica.service.vo.CadastroAlbumVO;
import br.com.torresti.musica.service.vo.ConsultaAlbumVO;
import br.com.torresti.musica.service.vo.ListaMusicaVO;
import br.com.torresti.musica.util.MusicaConstantes.TipoSite;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.to.CodigoDescricaoTO;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class DownloadViewComponent {
	
	@Autowired
	private AlbumComponent albumComponent;
	
	/** - CADASTRO - **/
	
	public CadastroAlbumVO carregarCadastro() {
		return new CadastroAlbumVO(MenuTela.DownloadCadastroAlbum);
	}
	
	public void salvarCadastro(CadastroAlbumVO cadastroAlbumTO) throws BusinessException {
		ViewUtil.isNull(cadastroAlbumTO.getTipoSite(), "Favor informar o tipo site!");
		ViewUtil.isEmpty(cadastroAlbumTO.getUrlSite(), "Favor informar a url site!");
		albumComponent.salvar(cadastroAlbumTO.getTipoSite(), cadastroAlbumTO.getUrlSite());
	}
	
	/** - CONSULTA - **/
		
	public ConsultaAlbumVO carregarTela() {
		return carregar(albumComponent.consultarUltimo().getIdAlbum());
	}
	
	private ConsultaAlbumVO carregar(Integer idAlbum) {
		
		ConsultaAlbumVO consultaAlbumDTO = new ConsultaAlbumVO(MenuTela.DownloadConsultaAlbum);
		
		consultaAlbumDTO.setIdUltimoAlbum(albumComponent.consultarUltimo().getIdAlbum());
		
		Album album = albumComponent.consultar(idAlbum);
		
		if (album == null) {
			album = albumComponent.consultar(consultaAlbumDTO.getIdUltimoAlbum());
		}
		
		BeanUtils.copyProperties(album, consultaAlbumDTO);
		
		consultaAlbumDTO.setTipoSite(TipoSite.Decks.toString());
		
		List<ArtistaMusica> listaArtistaMusica = albumComponent.listarArtistaMusica(idAlbum);
		
		consultaAlbumDTO.setListaArtistaMusicaVO(albumComponent.ajustarListaArtistaMusica(listaArtistaMusica, false));
		
		List<CodigoDescricaoTO<Integer>> listaTag = new ArrayList<>();
		for (TagDiscoArtistaMusica tagDiscoArtistaMusica : TagDiscoArtistaMusica.values()) {
			listaTag.add(new CodigoDescricaoTO<Integer>(tagDiscoArtistaMusica.getCodigo(), tagDiscoArtistaMusica.getDescricao()));
		}
		
		consultaAlbumDTO.setListaTag(listaTag);
						
		return consultaAlbumDTO;
		
	}
	
	public ConsultaAlbumVO consultar(ConsultaAlbumVO consultaAlbumDTO) {
		return carregar(consultaAlbumDTO.getIdAlbum());
	}
	
	public ConsultaAlbumVO consultar(Integer idAlbum) {
		return carregar(idAlbum);
	}

	public ConsultaAlbumVO consularPrimeiroCadastrado() {
		return carregar(albumComponent.consularPrimeiroCadastrado());
	}
	
	public ConsultaAlbumVO consultarUltimoCadastrado() {
		return carregar(albumComponent.consularUltimoCadastrado());
	}
	
	public void alterarSituacao(ConsultaAlbumVO consultaAlbumDTO) {
		albumComponent.alterarSituacao(consultaAlbumDTO.getIdArtistaMusica(), consultaAlbumDTO.getSituacao());
	}
	
	public void alterarCategoria(ConsultaAlbumVO consultaAlbumDTO) {
		albumComponent.alterarCategoria(consultaAlbumDTO.getIdArtistaMusica(), consultaAlbumDTO.getCategoria());
	}
		
	public ConsultaAlbumVO excluirArtistaMusica(ConsultaAlbumVO consultaAlbumDTO) {
		
		try {

			albumComponent.excluirArtistaMusica(consultaAlbumDTO.getIdArtistaMusica());
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
		} catch (Exception e) {
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
			consultaAlbumDTO.setMensagemErro("Erro ao excluir musica! " + e.getMessage());			
			
		}
		
		return consultaAlbumDTO;
		
	} 
	
	public ConsultaAlbumVO excluirTagArtistaMusica(ConsultaAlbumVO consultaAlbumDTO) {
		
		try {

			albumComponent.excluirTagArtistaMusica(consultaAlbumDTO.getIdTagArtistaMusica());
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
		} catch (Exception e) {
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
			consultaAlbumDTO.setMensagemErro("Erro ao excluir Tag Artista! " + e.getMessage());			
			
		}
		
		return consultaAlbumDTO;
		
	} 
	
	public ConsultaAlbumVO inserirTagArtistaMusica(ConsultaAlbumVO consultaAlbumDTO) {
		
		try {

			albumComponent.inserirTagArtistaMusica(consultaAlbumDTO.getIdArtistaMusica(), consultaAlbumDTO.getIdTag());
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
		} catch (Exception e) {
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
			consultaAlbumDTO.setMensagemErro("Erro ao incluir Tag Artista! " + e.getMessage());			
			
		}
		
		return consultaAlbumDTO;
		
	} 
	
	public ConsultaAlbumVO finalizar(ConsultaAlbumVO consultaAlbumDTO) {
		
		try {

			albumComponent.finalizar(consultaAlbumDTO.getIdAlbum());
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
		} catch (Exception e) {
			
			consultaAlbumDTO = carregar(consultaAlbumDTO.getIdAlbum());
			
			consultaAlbumDTO.setMensagemErro("Erro ao finalizadar Album! " + e.getMessage());			
			
		}
		
		return consultaAlbumDTO;
		
	} 
	
	public void salvarMusica(ConsultaAlbumVO consultaAlbumDTO) {
		albumComponent.salvarMusica(consultaAlbumDTO.getIdArtistaMusica(), consultaAlbumDTO.getArtista(), consultaAlbumDTO.getMusica());
	}
	
	/** - BAIXA - **/
	
	public ListaMusicaVO carregarBaixaAlbum() {
		return consultar(new ListaMusicaVO(MenuTela.DownloadBaixaAlbum));
	}
	
	public ListaMusicaVO consultar(ListaMusicaVO listaMusicaVO) {
		
		try {
			
			listaMusicaVO.recarregarTitulo(MenuTela.DownloadBaixaAlbum);

			List<ArtistaMusica> listaArtistaMusica = new ArrayList<ArtistaMusica>();
			
			if (listaMusicaVO.getSituacao() != null) {
				
				if (listaMusicaVO.getIdTag() != null) {
					
					listaArtistaMusica = albumComponent.listarArtistaMusicalistar(listaMusicaVO.getIdTag(), listaMusicaVO.getSituacao(), listaMusicaVO.getCategoria());
					
				} else {
					
					listaArtistaMusica = albumComponent.listarArtistaMusicalistar(listaMusicaVO.getSituacao(), listaMusicaVO.getCategoria());
					
				}
				
				listaMusicaVO.setListaArtistaMusicaVO(albumComponent.ajustarListaArtistaMusica(listaArtistaMusica, true));
				
			} else {
				
				listaMusicaVO.setSituacao(1);
				
			}
						
		} catch (Exception e) {
			listaMusicaVO.setMensagemErro("Erro ao consultar! " + e.getMessage());			
		}
		
		return listaMusicaVO;
		
	}
	
	public void alterarSituacao(Integer idArtistaMusica, Integer situacao) {
		albumComponent.alterarSituacao(idArtistaMusica, situacao);
	}		
	
	
}
