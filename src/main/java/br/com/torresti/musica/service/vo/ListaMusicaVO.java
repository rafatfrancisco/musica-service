package br.com.torresti.musica.service.vo;

import java.util.ArrayList;
import java.util.List;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.spring.to.BaseTO;

public class ListaMusicaVO extends BaseTO {

	private Integer idArtistaMusica;

	private Integer situacao;

	private Integer categoria;

	private Integer idTag;

	private List<ArtistaMusicaVO> listaArtistaMusicaVO = new ArrayList<ArtistaMusicaVO>();

	private Integer situacaoMusica;
	
	public ListaMusicaVO() {}
	
	public ListaMusicaVO(MenuTela menuTela) {
		super(menuTela.getTitulo(), menuTela.getCaminho());
	}
	
	public void recarregarTitulo(MenuTela menuTela) {
		this.setTitulo(menuTela.getTitulo());
		this.setCaminho(menuTela.getCaminho());
	}

	public Integer getIdArtistaMusica() {
		return idArtistaMusica;
	}

	public void setIdArtistaMusica(Integer idArtistaMusica) {
		this.idArtistaMusica = idArtistaMusica;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getIdTag() {
		return idTag;
	}

	public void setIdTag(Integer idTag) {
		this.idTag = idTag;
	}

	public List<ArtistaMusicaVO> getListaArtistaMusicaVO() {
		return listaArtistaMusicaVO;
	}

	public void setListaArtistaMusicaVO(List<ArtistaMusicaVO> listaArtistaMusicaVO) {
		this.listaArtistaMusicaVO = listaArtistaMusicaVO;
	}

	public Integer getSituacaoMusica() {
		return situacaoMusica;
	}

	public void setSituacaoMusica(Integer situacaoMusica) {
		this.situacaoMusica = situacaoMusica;
	}

}
