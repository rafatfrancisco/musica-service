package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.musica.util.dto.DiscoDTO;
import br.com.torresti.spring.to.BaseTO;
import br.com.torresti.spring.to.CodigoDescricaoTO;

public class OrganizaDiscoVO extends BaseTO {

	private Integer idDisco;

	private Integer idPrateleira;
	
	private List<CodigoDescricaoTO<Integer>> listaPrateleira;

	private List<DiscoDTO> listaDiscoDTO;

	public OrganizaDiscoVO() {}

	public OrganizaDiscoVO(MenuTela menuTela) {
		super(menuTela.getTitulo(), menuTela.getCaminho());
	}

	public void recarregarTitulo(MenuTela menuTela) {
		this.setTitulo(menuTela.getTitulo());
		this.setCaminho(menuTela.getCaminho());
	}

	public Integer getIdDisco() {
		return idDisco;
	}

	public void setIdDisco(Integer idDisco) {
		this.idDisco = idDisco;
	}

	public Integer getIdPrateleira() {
		return idPrateleira;
	}

	public void setIdPrateleira(Integer idPrateleira) {
		this.idPrateleira = idPrateleira;
	}

	public List<CodigoDescricaoTO<Integer>> getListaPrateleira() {
		return listaPrateleira;
	}

	public void setListaPrateleira(List<CodigoDescricaoTO<Integer>> listaPrateleira) {
		this.listaPrateleira = listaPrateleira;
	}

	public List<DiscoDTO> getListaDiscoDTO() {
		return listaDiscoDTO;
	}

	public void setListaDiscoDTO(List<DiscoDTO> listaDiscoDTO) {
		this.listaDiscoDTO = listaDiscoDTO;
	}

}
