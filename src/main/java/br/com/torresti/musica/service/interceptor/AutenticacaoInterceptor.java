package br.com.torresti.musica.service.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.torresti.musica.util.DateUtil;

public class AutenticacaoInterceptor extends HandlerInterceptorAdapter {
	
	private static final String TMS_USER = "torresti";
	private static final String TMS_PASSWORD = "";
	private static final String PATH_JSP = "/WEB-INF/view/jsp/";
	private static final String USUARIO_AUTENTICADO = "autenticado";
	private static final String PARAMETRO_USU = "usuario";
	private static final String PARAMETRO_PWD = "senha";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		/*
		String usuario = request.getParameter(PARAMETRO_USU);
		String senha = request.getParameter(PARAMETRO_PWD);
		Boolean usuarioAutenticado = (Boolean) request.getSession().getAttribute(USUARIO_AUTENTICADO);
		if (usuarioAutenticado != null && usuarioAutenticado == true) {
			return true;
		} else if (!this.senhaValida(usuario, senha)) {
			request.getRequestDispatcher(PATH_JSP + "login.jsp").forward(request, response);
			request.getSession().setAttribute(USUARIO_AUTENTICADO, false);
			return false;
		} else {
			request.getSession().setAttribute(USUARIO_AUTENTICADO, true);
			return true;
		}
		*/	
		return true;
	}
	
	private boolean senhaValida(String usuario, String senha) {
		if (senha != null && senha.length() > 0) {
			String senhaDeHoje = TMS_PASSWORD + DateUtil.getDiaDoMes();
			if (TMS_USER.equals(usuario) && senha.toLowerCase().equals(senhaDeHoje)) {
				return true;
			}
		}
		return false;
	}
	
}
