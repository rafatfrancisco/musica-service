package br.com.torresti.musica.service.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseRepository<T, ID extends Serializable> {
	
	@PersistenceContext//(unitName = "musica-crud")
	protected EntityManager entityManager;

}
