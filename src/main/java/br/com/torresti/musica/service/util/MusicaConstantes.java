package br.com.torresti.musica.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MusicaConstantes {

	public static enum TagDiscoArtistaMusica {
		
		Dub(20,"Dub"),
		Deep(21,"Deep"),
		House(22,"House"),
		Detroit(23,"Detroit"),
		Minimal(24,"Minimal"),
		Techno(25,"Techno"),
		Techouse(26,"Techouse"),
		Break(27,"Break"),
		Romeno(28,"Romeno"),
		Acapella(100,"Acapella"),
		Tools(101,"Tools"),
		Intro(102,"Intro"),
		Lounge(103,"Lounge"),
		Villalobos(200,"Villalobos"),
		Raresh(201,"Raresh"),
		Zip(202,"Zip"),
		Garnier(203,"Garnier"),
		SvenVath(204,"Vath"),
		RomenoMinimal(1001,"Romeno / Minimal", 28, 24),
		RomenoTechouse(1002,"Romeno / Techouse", 28, 26),
		RomenoDeep(1003,"Romeno / Deep", 28, 21),
		RomenoBreak(1004,"Romeno / Break", 28, 27),
		TechouseBreak(1005,"Techouse / Break", 26, 27);
		
		private Integer codigo;
		
		private String descricao;
		
		private List<Integer> listaComplemento;
	
		private TagDiscoArtistaMusica(Integer codigo, String descricao) {
			this.codigo = codigo;
			this.descricao = descricao;
			this.listaComplemento = new ArrayList<>();
		}
		
		private TagDiscoArtistaMusica(Integer codigo, String descricao, Integer... listaComplemento) {
			this.codigo = codigo;
			this.descricao = descricao;
			this.listaComplemento = Arrays.asList(listaComplemento);
		}

		public Integer getCodigo() {
			return codigo;
		}

		public void setCodigo(Integer codigo) {
			this.codigo = codigo;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public List<Integer> getListaComplemento() {
			return listaComplemento;
		}

		public void setListaComplemento(List<Integer> listaComplemento) {
			this.listaComplemento = listaComplemento;
		}
		
		public static TagDiscoArtistaMusica getByCodigo(Integer codigo) {
			for (TagDiscoArtistaMusica tipo : TagDiscoArtistaMusica.values()) {
				if (codigo.equals(tipo.codigo)) {
					return tipo;
				}
			}
			return null;
		}
			
	}
	
}
