package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.spring.to.BaseTO;
import br.com.torresti.spring.to.CodigoDescricaoTO;

public class ConsultaAlbumVO extends BaseTO {

	private Integer idUltimoAlbum;

	private Integer idAlbum;

	private Integer idArtistaMusica;

	private Integer idTagArtistaMusica;

	private Integer idTag;

	private String tipoSite;

	private String urlSite;

	private String nome;

	private Integer ano;

	private String selo;

	private Integer situacao;

	private List<ArtistaMusicaVO> listaArtistaMusicaVO;

	private Integer categoria;

	private String artista;

	private String musica;
	
	private List<CodigoDescricaoTO<Integer>> listaTag;
	
	public ConsultaAlbumVO() {}
	
	public ConsultaAlbumVO(MenuTela menuTela) {
		this.setTitulo(menuTela.getTitulo());
		this.setCaminho(menuTela.getCaminho());
	}

	public Integer getIdUltimoAlbum() {
		return idUltimoAlbum;
	}

	public void setIdUltimoAlbum(Integer idUltimoAlbum) {
		this.idUltimoAlbum = idUltimoAlbum;
	}

	public Integer getIdAlbum() {
		return idAlbum;
	}

	public void setIdAlbum(Integer idAlbum) {
		this.idAlbum = idAlbum;
	}

	public Integer getIdArtistaMusica() {
		return idArtistaMusica;
	}

	public void setIdArtistaMusica(Integer idArtistaMusica) {
		this.idArtistaMusica = idArtistaMusica;
	}

	public Integer getIdTagArtistaMusica() {
		return idTagArtistaMusica;
	}

	public void setIdTagArtistaMusica(Integer idTagArtistaMusica) {
		this.idTagArtistaMusica = idTagArtistaMusica;
	}

	public Integer getIdTag() {
		return idTag;
	}

	public void setIdTag(Integer idTag) {
		this.idTag = idTag;
	}

	public String getTipoSite() {
		return tipoSite;
	}

	public void setTipoSite(String tipoSite) {
		this.tipoSite = tipoSite;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getSelo() {
		return selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public List<ArtistaMusicaVO> getListaArtistaMusicaVO() {
		return listaArtistaMusicaVO;
	}

	public void setListaArtistaMusicaVO(List<ArtistaMusicaVO> listaArtistaMusicaVO) {
		this.listaArtistaMusicaVO = listaArtistaMusicaVO;
	}

	public Integer getCategoria() {
		return categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public List<CodigoDescricaoTO<Integer>> getListaTag() {
		return listaTag;
	}

	public void setListaTag(List<CodigoDescricaoTO<Integer>> listaTag) {
		this.listaTag = listaTag;
	}
	
}
