package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.MusicaDisco;

@Repository
public interface MusicaDiscoRepository extends CrudRepository<MusicaDisco, Integer> {
	
	@Query("SELECT p FROM MusicaDisco p WHERE p.disco.idDisco = :idDisco ")
	public List<MusicaDisco> listar(@Param("idDisco") Integer idDisco);

}
