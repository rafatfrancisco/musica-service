package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.torresti.musica.crud.model.entidy.TagArtistaMusica;

@Repository
@Transactional
public interface TagArtistaMusicaRepository extends CrudRepository<TagArtistaMusica, Long> {
	
	@Query("SELECT p FROM TagArtistaMusica p WHERE p.idTagArtistaMusica = :idTagArtistaMusica")
	public TagArtistaMusica consular(@Param("idTagArtistaMusica") Integer idTagArtistaMusica);
	
	@Query("SELECT p FROM TagArtistaMusica p WHERE p.artistaMusica.idArtistaMusica = :idArtistaMusica")
	public List<TagArtistaMusica> listar(@Param("idArtistaMusica") Integer idArtistaMusica);

}
