package br.com.torresti.musica.service.component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.crud.model.entidy.Disco;
import br.com.torresti.musica.crud.model.entidy.MusicaDisco;
import br.com.torresti.musica.crud.model.entidy.Prateleira;
import br.com.torresti.musica.service.repository.DiscoRepository;
import br.com.torresti.musica.service.repository.PrateleiraRepository;
import br.com.torresti.musica.service.vo.MusicaDiscoVO;
import br.com.torresti.musica.util.DateUtil;
import br.com.torresti.musica.util.MusicaConstantes.TipoSite;
import br.com.torresti.musica.util.MusicaUtil;
import br.com.torresti.musica.util.ValidacaoUtil;
import br.com.torresti.musica.util.dto.DiscoDTO;
import br.com.torresti.musica.util.dto.MusicaDiscoDTO;
import br.com.torresti.musica.util.dto.PesquisaDiscoDTO;
import br.com.torresti.musica.util.dto.ResultadoDiscoDTO;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.ArtistaMusicaVO;
import br.com.torresti.musica.util.to.CompraDiscoTO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.to.CodigoDescricaoTO;
import br.com.torresti.spring.to.RestTO;

@Component
public class DiscoComponent extends MusicaComponent {

	@Autowired
	private DiscoRepository discoRepository;

	@Autowired
	private PrateleiraRepository prateleiraRepository;

	@Autowired
	private LojaComponent lojaComponent;
	
	/*
	private String urlSite(Integer tipo, String id) throws BusinessException {

		if (tipo == 1) {
			return discogsComponent.urlFindTrack(id);
		}

		if (tipo == 2) {
			return decksComponent.urlFindTrack(id);
		}
		
		if (tipo == 3) {
			return junoComponent.urlFindTrack(id);
		}

		throw new BusinessException("Tipo inv�lido!");

	}
	
	public String consultarImagem(Integer tipo, String url, String urlImagemPesquisada) throws BusinessException {
		
		String urlImagem = null; 
		
		if (tipo == 1) {
			urlImagem = discogsComponent.consultarImagem(url);
		}

		if (tipo == 2) {
			urlImagem = decksComponent.consultarImagem(url);
		}
		
		if (tipo == 3) {
			urlImagem = urlImagemPesquisada;
		}
		
		if (urlImagem != null) {
			return urlImagem;
		} else {
			return "http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg";
		}
		
	}
	*/

	public void salvarCompraDisco(CompraDiscoTO compraDiscoTO) throws BusinessException {

		String idSite = new String();

		try {

			List<AlbumTO> listaAlbumTO = new ArrayList<AlbumTO>();

			for (String id : compraDiscoTO.getListaId()) {

				idSite = id;

				listaAlbumTO.add(lojaComponent.consultarId(compraDiscoTO.getTipo(), id));

			}

			idSite = new String();

			Prateleira prateleira = entityManager.find(Prateleira.class, compraDiscoTO.getIdPrateleira());

			Date data = DateUtil.parseData(compraDiscoTO.getData());

			int index = 0;

			for (AlbumTO albumTO : listaAlbumTO) {

				idSite = compraDiscoTO.getListaId().get(index);

				ValidacaoUtil.validar(albumTO);

				Disco disco = new Disco();
				disco.setUrlSite(MusicaUtil.obterIdToURL(compraDiscoTO.getTipo(), idSite));
				disco.setCodigo(idSite);
				disco.setTipo(compraDiscoTO.getTipo());
				disco.setUrlImagem(albumTO.getUrlImagem());
				disco.setDataCriacao(data);
				disco.setNome(albumTO.getAlbum());
				disco.setAno(new Integer(albumTO.getAno()));
				disco.setSelo(albumTO.getSelo());
				disco.setSituacao(0);
				disco.setPrateleira(prateleira);
				
				this.ajustarLinkErradoImagem(disco, false, "https://gfx2.decks.de", "https://gfx1.decks.de", "https://gfx7.decks.de");

				entityManager.persist(disco);

				for (ArtistaMusicaVO artistaMusicaVO : albumTO.getListaArtistaMusicaTO()) {

					MusicaDisco musicaDisco = new MusicaDisco();
					musicaDisco.setDisco(disco);
					musicaDisco.setArtista(artistaMusicaVO.getArtista());
					musicaDisco.setMusica(artistaMusicaVO.getMusica());
					musicaDisco.setCategoria(0);
					entityManager.persist(musicaDisco);

				}

				index++;

			}

		} catch (Exception e) {

			if (idSite != null) {

				throw new BusinessException("Erro ao processar id=" + idSite + ", erro " + e.getMessage());

			} else {

				throw new BusinessException("Erro geral, " + e.getMessage());

			}

		}

	}

	public void carregarDiscoPrincipal(Disco disco, ResultadoDiscoDTO resultadoDiscoDTO) throws BusinessException {
		
		if (disco.getPrateleira() != null) {
			
			List<Integer> listaIdDisco = discoRepository.consultarDiscoPrateleira(disco.getPrateleira().getIdPrateleira());
			
			int total = listaIdDisco.size();
			
			int posicao = 0;
			
			for (int i = 0; i < listaIdDisco.size(); i++) {
				if (disco.getIdDisco() == listaIdDisco.get(i)) {
					posicao = i + 1;
				}
			}
			
			int divisaoPrateleira = obterPosicao(total, posicao);
			
			int divisaoExataPrateleira = obterPosicao(total / 3, posicao / divisaoPrateleira);
			
			resultadoDiscoDTO.setTotalPrateleira(total);
			
			resultadoDiscoDTO.setPosicaoPrateleira(posicao);
						
			resultadoDiscoDTO.setNomePrateleira(disco.getPrateleira().getNome() + " " + obterPosicao(divisaoPrateleira) + " esta no " + obterPosicao(divisaoExataPrateleira));
			
		}
		
		resultadoDiscoDTO.setDiscoDTO(this.converter(disco, false));
				
	}

	public static int obterPosicao(int total, int posicao) {
		
        int divisao = total / 3;
        
        int inicio = 0;
        
        for (int i = 1; i < 4; i++) {
			
        	int fim = inicio + divisao;
        	
        	if (posicao >= inicio && posicao <= fim) {
        		
        		return i;
        		
        	}
        	        	
        	inicio = fim;
        	
		}
        
        return 0;
		
	}
	
	public static String obterPosicao(int posicao) {
		switch (posicao) {
			case 1: return "01";
			case 2: return "02";
			case 3: return "03";
			default: return "Erro";
		}
	}
	
	public List<DiscoDTO> pesquisarDiscoCase(String id, String codigo, String selo, String artista, String musica, String nome) {
		
		List<DiscoDTO> listaDiscoDTO = new ArrayList<>();
		
		List<Disco> listaDisco = discoRepository.consultar(id, codigo, selo, artista, musica, nome);
		
		for (Disco disco : listaDisco) {
			listaDiscoDTO.add(this.converter(disco, true));
		}
		
		return listaDiscoDTO;
		
	}
	
	public List<ResultadoDiscoDTO> pesquisarDiscoCase(Integer idDisco) {
		
		List<ResultadoDiscoDTO> retorno = new ArrayList<ResultadoDiscoDTO>();
		
		Disco disco = entityManager.find(Disco.class, idDisco);
		
		PesquisaDiscoDTO pesquisaDiscoDTO = new PesquisaDiscoDTO();
		pesquisaDiscoDTO.setQuantidade(10);
		
		ResultadoDiscoDTO resultadoDiscoDTO = this.pesquisarDiscoCase(pesquisaDiscoDTO, disco);
		retorno.add(resultadoDiscoDTO);
		
		return retorno;
	}
	
	public PesquisaDiscoDTO pesquisarDiscoCase(PesquisaDiscoDTO pesquisaDiscoDTO) throws BusinessException {
		
		pesquisaDiscoDTO.setListaResultadoDiscoDTO(new ArrayList<ResultadoDiscoDTO>());
		
		List<Disco> listaDisco = discoRepository.consultar(pesquisaDiscoDTO.getId(), pesquisaDiscoDTO.getCodigoUrl(), pesquisaDiscoDTO.getSelo(), pesquisaDiscoDTO.getArtista(), pesquisaDiscoDTO.getMusica(), null);
		
		for (Disco disco : listaDisco) {
			
			pesquisaDiscoDTO.getListaResultadoDiscoDTO().add(this.pesquisarDiscoCase(pesquisaDiscoDTO, disco));
			
		}
		
		return pesquisaDiscoDTO;

	}
	
	private ResultadoDiscoDTO pesquisarDiscoCase(PesquisaDiscoDTO pesquisaDiscoDTO, Disco disco) {
		
		ResultadoDiscoDTO resultadoDiscoDTO = new ResultadoDiscoDTO();
		
		List<Disco> listaDiscoMenor = discoRepository.consultarMenor(disco.getIdDisco(), disco.getPrateleira(), pesquisaDiscoDTO.getQuantidade());
		for (Disco discoMenor : listaDiscoMenor) {
			resultadoDiscoDTO.getListaDiscoMenorDTO().add(this.converter(discoMenor, false));
		}
		
		if (!resultadoDiscoDTO.getListaDiscoMenorDTO().isEmpty()) {
			resultadoDiscoDTO.getListaDiscoMenorDTO().get(0).setActive("active");
		}

		this.carregarDiscoPrincipal(disco, resultadoDiscoDTO);
		
		List<Disco> listaDiscoMaior = discoRepository.consultarMaior(disco.getIdDisco(), disco.getPrateleira(), pesquisaDiscoDTO.getQuantidade());
		for (Disco discoMaior : listaDiscoMaior) {
			resultadoDiscoDTO.getListaDiscoMaiorDTO().add(this.converter(discoMaior, false));
		}
		
		if (!resultadoDiscoDTO.getListaDiscoMaiorDTO().isEmpty()) {
			resultadoDiscoDTO.getListaDiscoMaiorDTO().get(0).setActive("active");
		}
		
		pesquisaDiscoDTO.getListaResultadoDiscoDTO().add(resultadoDiscoDTO);
		
		return resultadoDiscoDTO;
		
	}
	
	private DiscoDTO converter(Disco disco, boolean carregaMusica) {
		
		DiscoDTO discoDTO = new DiscoDTO();
		discoDTO.setIdDisco(disco.getIdDisco());
		discoDTO.setUrlSite(this.converterUrlSite(disco.getUrlSite()));
		discoDTO.setSituacao(disco.getSituacao());
		discoDTO.setNome(disco.getNome());
		discoDTO.setAno(disco.getAno());
		discoDTO.setTipo(disco.getTipo());
		discoDTO.setSelo(disco.getSelo());
		discoDTO.setUrlImagem(disco.getUrlImagem());
		discoDTO.setIdPrateleira(disco.getPrateleira() != null ? disco.getPrateleira().getIdPrateleira() : null);
		discoDTO.setDataCriacao(DateUtil.formataSemHora(disco.getDataCriacao()));
		
		if (carregaMusica) {
			discoDTO.setListaMusicaDiscoDTO(disco.getListaDiscoMusica().stream().map(e -> new MusicaDiscoDTO(e.getArtista(), e.getMusica())).collect(Collectors.toList()));	
		}

		return discoDTO;
		
	}
	
	private String converterUrlSite(String urlSite) {
		if (urlSite.indexOf("https://api.discogs.com/releases/") != -1) {
			urlSite = urlSite.replace("https://api.discogs.com/releases/", "https://discogs.com/release/");
		}
		return urlSite;
	}
	
	public List<CodigoDescricaoTO<Integer>> listarPrateleira() {
		return prateleiraRepository.listar().stream().map(e -> new CodigoDescricaoTO<Integer>(e.getIdPrateleira(), e.getNome())).collect(Collectors.toList());
	}

	public List<DiscoDTO> consultarOrganizaDisco(Integer idDisco) {
		
		List<DiscoDTO> listaDiscoDTO = new ArrayList<>();
		
		List<Disco> listaDiscoMaior = discoRepository.consultarMaior(idDisco, null, 10);
		
		for (Disco discoMaior : listaDiscoMaior) {
			
			listaDiscoDTO.add(this.converter(discoMaior, true));
			
		}
		
		return listaDiscoDTO;
				
	}
	
	public void ajustarDisco(List<DiscoDTO> listaDiscoDTO) {
		
		for (DiscoDTO discoDTO : listaDiscoDTO) {
			
			Disco disco = entityManager.find(Disco.class, discoDTO.getIdDisco());
			disco.setSituacao(discoDTO.getSituacao());
			disco.setPrateleira(discoDTO.getIdPrateleira() != null ? entityManager.find(Prateleira.class, discoDTO.getIdPrateleira()) : null);

			entityManager.merge(disco);
			
		}
		
	} 
	
	public void ajustarLinkErradoImagem() {
		
		List<Disco> listaDisco = discoRepository.listar();
		
		for (Disco disco : listaDisco) {
			
			this.ajustarLinkErradoImagem(disco, true, "https://gfx2.decks.de", "https://gfx1.decks.de", "https://gfx7.decks.de");
			
		}
		
	}
	
	private void ajustarLinkErradoImagem(Disco disco, boolean ajusta, String para, String... lista) {
		for (String de : lista) {
			if (disco.getUrlImagem().indexOf(de) != -1) {
				disco.setUrlImagem(disco.getUrlImagem().replace(de, para));
				if (ajusta) {
					entityManager.merge(disco);
				}
				break;
			}			
		}
	}

	/*
	public void processarImagem(Integer idDisco, Integer quantidade) {
		
		List<Disco> listaDisco = discoRepository.consultarMaiorSemImagem(idDisco, quantidade);
		
		for (Disco disco : listaDisco) {
			 
			disco.setUrlImagem(this.consultarImagem(disco.getTipo(), disco.getUrlSite(), null));
			
			entityManager.merge(disco);
			
		}
		
	}
	
	public void ajustarRecadastro(Integer idDisco) {
		
		List<Integer> lista = discoRepository.consultarMenor(idDisco);
		
		for (Integer id : lista) {
			
			this.clonar(id);
			
			this.excluir(id);
						
		}
		
	}

	public void clonar(Integer idDisco) {
		
		Disco discoPesquisado = entityManager.find(Disco.class, idDisco);
		
		List<MusicaDisco> listaMusicaDisco = new ArrayList<>();
		
		for (MusicaDisco musicaDisco : discoPesquisado.getListaDiscoMusica()) {
			
			MusicaDisco musicaDiscoNovo = new PersistenceCloner<MusicaDisco>(musicaDisco).generateCopyToPersist();
			
			musicaDiscoNovo.setDisco(null);
			
			listaMusicaDisco.add(musicaDiscoNovo);
			
		}
		 		
		Armario armario = discoPesquisado.getArmario();
		
		Divisoria divisoria = discoPesquisado.getDivisoria();
		
		Prateleira prateleira = discoPesquisado.getPrateleira();
		
		Disco disco = new PersistenceCloner<Disco>(discoPesquisado).generateCopyToPersist();
		
		disco.setIdDisco(null);
		
		disco.setArmario(armario);
		
		disco.setDivisoria(divisoria);
		
		disco.setPrateleira(prateleira);
				
		entityManager.persist(disco);
		
		for (MusicaDisco musicaDisco : listaMusicaDisco) {
			
			musicaDisco.setDisco(disco);
						
			entityManager.persist(musicaDisco);
						
		}	
		
	}
	*/
	
	public void excluir(Integer idDisco) {
		Disco disco = this.obterDisco(idDisco);
		for (MusicaDisco musicaDisco : disco.getListaDiscoMusica()) {
			entityManager.remove(musicaDisco);
		}
		entityManager.remove(disco);
	}
	
	public Disco obterDisco(Integer idDisco) {
		return entityManager.find(Disco.class, idDisco);
	}
	
	public void salvarMusicaDisco(List<MusicaDiscoVO> listaMusicaDiscoVO) {
		for (MusicaDiscoVO musicaDiscoVO : listaMusicaDiscoVO) {
			MusicaDisco musicaDisco = entityManager.find(MusicaDisco.class, musicaDiscoVO.getIdMusicaDisco());
			musicaDisco.setArtista(musicaDiscoVO.getArtista());
			musicaDisco.setMusica(musicaDiscoVO.getMusica());
			entityManager.merge(musicaDisco);
		}
	}

	// -- PROCESSO -- //

	public RestTO ajustarImagemDecks() {
		
		List<Disco> listaDisco = discoRepository.consultarTempAjustarImagem(TipoSite.Decks.getTipo(), "https://gfx8.decks.de/");
		
		for (Disco disco : listaDisco) {
			
			disco.setUrlImagem(disco.getUrlImagem().replace("https://gfx8.decks.de/", "https://gfx2.decks.de/"));
			
			entityManager.merge(disco);
			
		}
				
		return RestTO.sucesso();
		
	}
	
	public RestTO ajustarImagemDiscogs() {
		
		int total = 1;
		
		List<Disco> listaDisco = discoRepository.consultarTempAjustarImagem(TipoSite.Discogs.getTipo(), "http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg");
		
		for (Disco disco : listaDisco) {
			
			String id = MusicaUtil.obterIdToURL(TipoSite.Discogs.getTipo(), disco.getUrlSite());
			
			AlbumTO albumTO = lojaComponent.consultarId(disco.getTipo(), id);
			
			disco.setUrlImagem(albumTO.getUrlImagem());
			
			entityManager.merge(disco);
			
			total++;
			
			if (total == 10) break; 
			
		}
		
		
		return RestTO.sucesso();
		
	}
	
	public RestTO ajustarOrdem() {
				
		List<Integer> listaPrateleira = new ArrayList<Integer>();
		
		/*
		OK PT 02
		Integer idPrateleiraDestino = 2;
		listaPrateleira.add(2);
		*/
		
		/*
		OK PT 01
		Integer idPrateleiraDestino = 1;
		listaPrateleira.add(8);
		listaPrateleira.add(9);
		*/
	
		/*
		OK PT 06
		Integer idPrateleiraDestino = 16;
		listaPrateleira.add(3);
		*/
		
		/*
		OK PT 05
		Integer idPrateleiraDestino = 15;
		listaPrateleira.add(4);
		*/
		
		/*
		OK PT 04
		Integer idPrateleiraDestino = 14;
		listaPrateleira.add(5);
		listaPrateleira.add(6);
		*/
		
		/*
		OK PT 03
		*/
		Integer idPrateleiraDestino = 13;
		listaPrateleira.add(7);
				
		Integer idDisco = 1310;
		
		Prateleira prateleiraDestino = entityManager.find(Prateleira.class, idPrateleiraDestino);
		
		for (Integer idPrateleira : listaPrateleira) {
			
			for (Disco discoConsultado : discoRepository.consultarAjustarOrdemDeletar(idPrateleira)) {
				for (MusicaDisco musicaDiscoConsultado : discoConsultado.getListaDiscoMusica()) {
					entityManager.remove(musicaDiscoConsultado);
				}
				entityManager.remove(discoConsultado);				
			}

			List<Disco> listaDisco = discoRepository.consultarTempAjustarOrdem(idPrateleira);
			
			for (Disco discoConsultado : listaDisco) {

				Disco disco = new Disco();
				disco.setIdDisco(idDisco);
				disco.setCodigo(discoConsultado.getCodigo());//MusicaUtil.obterIdToURL(disco.getTipo(), disco.getUrlSite()));
				disco.setUrlSite(discoConsultado.getUrlSite());
				disco.setTipo(discoConsultado.getTipo());
				disco.setUrlImagem(discoConsultado.getUrlImagem());
				disco.setDataCriacao(discoConsultado.getDataCriacao());
				disco.setNome(discoConsultado.getNome());
				disco.setAno(discoConsultado.getAno());
				disco.setSelo(discoConsultado.getSelo());
				disco.setSituacao(discoConsultado.getSituacao());
				disco.setPrateleira(prateleiraDestino);
				
				entityManager.persist(disco);

				for (MusicaDisco musicaDiscoConsultado : discoConsultado.getListaDiscoMusica()) {

					MusicaDisco musicaDisco = new MusicaDisco();
					musicaDisco.setDisco(disco);
					musicaDisco.setArtista(musicaDiscoConsultado.getArtista());
					musicaDisco.setMusica(musicaDiscoConsultado.getMusica());
					musicaDisco.setCategoria(0);
					entityManager.persist(musicaDisco);
					entityManager.remove(musicaDiscoConsultado);
					
				}

				entityManager.remove(discoConsultado);
				
				idDisco++;

			}	
			
		}
						
		return RestTO.sucesso();
		
	}
		
}
