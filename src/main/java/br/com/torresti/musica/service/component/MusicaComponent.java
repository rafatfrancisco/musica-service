package br.com.torresti.musica.service.component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class MusicaComponent {
	
	@PersistenceContext//(unitName = "musica-crud")
	protected EntityManager entityManager;
		
}
