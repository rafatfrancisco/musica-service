package br.com.torresti.musica.service.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.torresti.musica.service.component.DownloadViewComponent;
import br.com.torresti.musica.service.vo.CadastroAlbumVO;
import br.com.torresti.musica.service.vo.ConsultaAlbumVO;
import br.com.torresti.musica.service.vo.ListaMusicaVO;
import br.com.torresti.spring.to.AjaxTO;

@Controller
@RequestMapping(value = "/download-view")
public class DownloadView {

	@Autowired
	private DownloadViewComponent downloadViewComponent;
	
	/** - CADASTRO - **/
	
	@RequestMapping(value = "/cadastro-album/carregar", method = RequestMethod.GET)
	public ModelAndView carregarCadastroAlbum() {
		return new ModelAndView("jsp/download/cadastro", "cadastroAlbumVO", downloadViewComponent.carregarCadastro());
	}
	
	@ResponseBody
	@RequestMapping(value = "/cadastro-album/salvar/ajax", method = RequestMethod.POST)
	public AjaxTO<Long> salvarCadastroAlbumAjax(@ModelAttribute("cadastroAlbumVO") CadastroAlbumVO cadastroAlbumVO) throws Exception {
		try {
			downloadViewComponent.salvarCadastro(cadastroAlbumVO);
			return AjaxTO.sucesso("Salvo com sucesso!");
		} catch (Exception e) {
			return AjaxTO.erro(e.getMessage());
		}
	}
	
	/** - CONSULTA - **/
		
	@RequestMapping(value = "/consulta-album/carregar", method = RequestMethod.GET)
	public ModelAndView carregarConsultaAlbum() {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO", downloadViewComponent.carregarTela());
	}

	@RequestMapping(value = "/consulta-album/consultar", method = RequestMethod.POST)
	public ModelAndView consultarConsultaAlbum(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.consultar(consultaAlbumVO));
	}
	
	@RequestMapping(value = "/consulta-album/consultar/{idAlbum}", method = RequestMethod.GET)
	public ModelAndView consultarConsultaAlbum(@PathVariable("idAlbum") Integer idAlbum) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.consultar(idAlbum));
	}

	@RequestMapping(value = "/consulta-album/consultar/primeiro/cadastrado", method = RequestMethod.GET)
	public ModelAndView consultarPrimeiroCadastradoConsultaAlbum() {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.consularPrimeiroCadastrado());
	}
	
	@RequestMapping(value = "/consulta-album/consultar/ultimo/cadastrado", method = RequestMethod.GET)
	public ModelAndView consultarUltimoCadastradoConsultaAlbum() {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.consultarUltimoCadastrado());
	}
	
	@ResponseBody
	@RequestMapping(value = "/consulta-album/alterar/situacao/ajax", method = RequestMethod.POST)
	public AjaxTO<Integer> alterarSituacaoConsultaAlbumAjax(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		try {
			downloadViewComponent.alterarSituacao(consultaAlbumVO);
			return AjaxTO.sucesso(1, null);
		} catch (Exception e) {
			return AjaxTO.erro(0,"Erro ao alterar a situ��o da m�sica!" + e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/consulta-album/alterar/categoria/ajax", method = RequestMethod.POST)
	public AjaxTO<Integer> alterarCategoriaConsultaAlbumAjax(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		try {
			downloadViewComponent.alterarCategoria(consultaAlbumVO);
			return AjaxTO.sucesso(null);
		} catch (Exception e) {
			return AjaxTO.erro("Erro ao alterar a categoria da m�sica!");
		}
	}	
	
	@RequestMapping(value = "/consulta-album/excluir/musica", method = RequestMethod.POST)
	public ModelAndView excluirArtistaMusicaConsultaAlbum(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.excluirArtistaMusica(consultaAlbumVO));
	}
	
	@RequestMapping(value = "/consulta-album/excluir/tag", method = RequestMethod.POST)
	public ModelAndView excluirTagArtistaMusicaConsultaAlbum(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.excluirTagArtistaMusica(consultaAlbumVO));
	}
	
	@RequestMapping(value = "/consulta-album/inserir/tag", method = RequestMethod.POST)
	public ModelAndView inserirTagArtistaMusica(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.inserirTagArtistaMusica(consultaAlbumVO));
	}

	@RequestMapping(value = "/consulta-album/finalizar", method = RequestMethod.POST)
	public ModelAndView finalizarConsultaAlbum(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		return new ModelAndView("jsp/download/consulta", "consultaAlbumVO",downloadViewComponent.finalizar(consultaAlbumVO));
	}
	
	@ResponseBody
	@RequestMapping(value = "/consulta-album/salvar/musica/ajax", method = RequestMethod.POST)
	public AjaxTO<Integer> salvarMusicaConsultaAlbumAjax(@ModelAttribute("consultaAlbumVO") ConsultaAlbumVO consultaAlbumVO) {
		try {
			downloadViewComponent.salvarMusica(consultaAlbumVO);
			return AjaxTO.sucesso("M�sica salva com sucesso!");
		} catch (Exception e) {
			return AjaxTO.erro(e.getMessage());
		}		
	}
	
	/** - BAIXA - **/
	
	@RequestMapping(value = "/baixa-album/carregar", method = RequestMethod.GET)
	public ModelAndView carregarBaixaAlbum() {
		return new ModelAndView("jsp/download/baixa", "listaMusicaVO", downloadViewComponent.carregarBaixaAlbum());
	}

	@RequestMapping(value = "/baixa-album/consultar", method = RequestMethod.POST)
	public ModelAndView consultarBaixaAlbum(@ModelAttribute("listaMusicaVO") ListaMusicaVO listaMusicaVO) {
		return new ModelAndView("jsp/download/baixa", "listaMusicaVO", downloadViewComponent.consultar(listaMusicaVO));
	}
	
	@ResponseBody
	@RequestMapping(value = "/baixa-album/alterar/situacao/ajax/{idArtistaMusica}/{situacao}", method = RequestMethod.GET)
	public AjaxTO<Integer> alterarSituacaoBaixaAlbumAjax(@PathVariable("idArtistaMusica") Integer idArtistaMusica, @PathVariable("situacao") Integer situacao) {
		try {
			downloadViewComponent.alterarSituacao(idArtistaMusica, situacao);
			return AjaxTO.sucesso("Situ��o da m�sica alterada com sucesso!");
		} catch (Exception e) {
			return AjaxTO.erro(e.getMessage());
		}
	}
	
}
