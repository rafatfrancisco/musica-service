package br.com.torresti.musica.service.vo;

import java.util.List;

import br.com.torresti.spring.to.BaseTO;

public class AlteraMusicaDiscoVO extends BaseTO {

	private List<MusicaDiscoVO> listaMusicaDiscoVO;

	public List<MusicaDiscoVO> getListaMusicaDiscoVO() {
		return listaMusicaDiscoVO;
	}

	public void setListaMusicaDiscoVO(List<MusicaDiscoVO> listaMusicaDiscoVO) {
		this.listaMusicaDiscoVO = listaMusicaDiscoVO;
	}
	
}
