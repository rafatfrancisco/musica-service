package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Album;

@Repository
public interface AlbumRepository extends CrudRepository<Album, Integer>{
	
	@Query("SELECT p FROM Album p ")
	public List<Album> listar();
	
	@Query("SELECT p FROM Album p WHERE p.idAlbum = (SELECT MAX(p.idAlbum) FROM Album p)")
	public Album consularUltimo();

	@Query("SELECT MIN(x.album.idAlbum) FROM ArtistaMusica x WHERE x.situacao = 0")
	public Integer consularPrimeiroCadastrado();
	
	@Query("SELECT MAX(x.album.idAlbum) FROM ArtistaMusica x WHERE x.situacao = 0")
	public Integer consularUltimoCadastrado();
	
	@Query("SELECT p FROM Album p WHERE p.idAlbum = :idAlbum")
	public Album consular(@Param("idAlbum") Integer idAlbum);
	
	@Query("SELECT p FROM Album p WHERE p.urlSite = :urlSite")
	public Album consular(@Param("urlSite") String urlSite);

}
