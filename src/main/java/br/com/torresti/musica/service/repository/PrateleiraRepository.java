package br.com.torresti.musica.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Prateleira;

@Repository
public interface PrateleiraRepository extends CrudRepository<Prateleira, Integer> {
	
	@Query("SELECT p FROM Prateleira p ")
	public List<Prateleira> listar();

}
