package br.com.torresti.musica.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torresti.musica.service.component.DiscoComponent;
import br.com.torresti.spring.to.RestTO;

@RestController
@RequestMapping(value = "/processo")
public class ProcessoController {

	@Autowired
	private DiscoComponent discoComponent;
	
	@GetMapping(value = "/ajustar/imagem")
	public ResponseEntity<RestTO> ajustarImagem() throws Exception {
		return new ResponseEntity<RestTO>(discoComponent.ajustarImagemDiscogs(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/ajustar/ordem")
	public ResponseEntity<RestTO> ajustarOrdem() throws Exception {
		return new ResponseEntity<RestTO>(discoComponent.ajustarOrdem(), HttpStatus.OK);
	}
	
}
