package br.com.torresti.musica.service.component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.crud.model.entidy.Album;
import br.com.torresti.musica.crud.model.entidy.ArtistaMusica;
import br.com.torresti.musica.crud.model.entidy.TagArtistaMusica;
import br.com.torresti.musica.service.repository.AlbumRepository;
import br.com.torresti.musica.service.repository.ArtistaMusicaRepository;
import br.com.torresti.musica.service.repository.TagArtistaMusicaRepository;
import br.com.torresti.musica.service.repository.TagRepository;
import br.com.torresti.musica.service.util.MusicaConstantes.TagDiscoArtistaMusica;
import br.com.torresti.musica.service.vo.ArtistaMusicaVO;
import br.com.torresti.musica.util.MusicaUtil;
import br.com.torresti.musica.util.StringUtil;
import br.com.torresti.musica.util.ValidacaoUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class AlbumComponent extends MusicaComponent {
	
	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private ArtistaMusicaRepository artistaMusicaRepository;
	
	@Autowired
	private TagArtistaMusicaRepository tagArtistaMusicaRepository;

	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private LojaComponent lojaComponent;
	
	public void salvar(Integer tipo, String url) throws BusinessException {
		
		ViewUtil.isNotNull(albumRepository.consular(url), "Album ja cadastrado para a URL " + url);
					
		AlbumTO albumTO = lojaComponent.consultarId(tipo, MusicaUtil.obterIdToURL(tipo, url));
		
		ValidacaoUtil.validar(albumTO);
		
		Album album = new Album();
		album.setUrlSite(url);
		album.setTipo(tipo);
		album.setDataCriacao(new Date());
		album.setNome(albumTO.getAlbum());
		album.setAno(new Integer(albumTO.getAno()));
		album.setSelo(albumTO.getSelo());
		entityManager.persist(album);
		
		for (br.com.torresti.musica.util.to.ArtistaMusicaVO artistaMusicaTO : albumTO.getListaArtistaMusicaTO()) {
			
			ArtistaMusica artistaMusica = new ArtistaMusica();
			artistaMusica.setAlbum(album);
			artistaMusica.setArtista(artistaMusicaTO.getArtista());
			artistaMusica.setMusica(artistaMusicaTO.getMusica());
			artistaMusica.setCategoria(0);
			artistaMusica.setSituacao(0);
			entityManager.persist(artistaMusica);
			
		}
		
	}
	
	public Album consultar(String urlDecks) {
		return albumRepository.consular(urlDecks);
	}
	
	public Album consultarUltimo() {
		return albumRepository.consularUltimo();
	}
	
	public Album consultar(Integer idAlbum) {
		return albumRepository.consular(idAlbum);
	}

	public Integer consularPrimeiroCadastrado() {
		return albumRepository.consularPrimeiroCadastrado();
	}
	
	public Integer consularUltimoCadastrado() {
		return albumRepository.consularUltimoCadastrado();
	}
	
	public ArtistaMusica consultarArtistaMusica(Integer idArtistaMusica) {
		return entityManager.find(ArtistaMusica.class, idArtistaMusica);
	}
	
	public void alterarSituacao(Integer idArtistaMusica, Integer situacao) {
		ArtistaMusica artistaMusica = this.consultarArtistaMusica(idArtistaMusica);
		artistaMusica.setSituacao(situacao);
		entityManager.merge(artistaMusica);
	}

	public void alterarCategoria(Integer idArtistaMusica, Integer categoria) {
		ArtistaMusica artistaMusica = this.consultarArtistaMusica(idArtistaMusica);
		artistaMusica.setCategoria(categoria);
		entityManager.merge(artistaMusica);
	}
	
	public void excluirArtistaMusica(Integer idArtistaMusica) {
		entityManager.remove(this.consultarArtistaMusica(idArtistaMusica));
	}
	
	public List<ArtistaMusica> listarArtistaMusica(Integer idAlbum) {
		return artistaMusicaRepository.listar(idAlbum);
	}
	
	public List<ArtistaMusica> listarArtistaMusicalistar(Integer situacao, Integer categoria) {
		return artistaMusicaRepository.listar(situacao, categoria);
	}
	
	public List<ArtistaMusica> listarArtistaMusicalistar(Integer idTag, Integer situacao, Integer categoria) {
		return artistaMusicaRepository.listar(idTag, situacao, categoria);
	}
	
	public List<ArtistaMusicaVO> ajustarListaArtistaMusica(List<ArtistaMusica> listaArtistaMusica, boolean mostraLink) {
		
		List<ArtistaMusicaVO> listaConsultaArtistaMusicaDTO = new ArrayList<ArtistaMusicaVO>();
		
		for (ArtistaMusica artistaMusica : listaArtistaMusica) {
			
			ArtistaMusicaVO artistaMusicaVO = new ArtistaMusicaVO();
			artistaMusicaVO.setIdArtistaMusica(artistaMusica.getIdArtistaMusica());
			artistaMusicaVO.setMusica(artistaMusica.getArtista() +  " - " + artistaMusica.getMusica());
			artistaMusicaVO.setArtistaOriginal(artistaMusica.getArtista());
			artistaMusicaVO.setMusicaOriginal(artistaMusica.getMusica());
			artistaMusicaVO.setQueryPesquisa(StringUtil.gerarQuery(artistaMusicaVO.getMusica()));
			artistaMusicaVO.setPesquisaMusica(artistaMusicaVO.getQueryPesquisa().replace("+", " "));
			
			if (mostraLink) {
				artistaMusicaVO.setLink(artistaMusica.getAlbum().getUrlSite());
			}
			
			artistaMusicaVO.setCategoria(artistaMusica.getCategoria());
			artistaMusicaVO.setSituacao(artistaMusica.getSituacao());
						
			List<TagArtistaMusica> listaTagArtistaMusica = this.listarTagArtistaMusica(artistaMusica.getIdArtistaMusica());
		
			for (TagArtistaMusica tagArtistaMusica : listaTagArtistaMusica) {
				tagArtistaMusica.getTag();
			}
			
			artistaMusicaVO.setListaTagArtistaMusica(listaTagArtistaMusica);
			
			listaConsultaArtistaMusicaDTO.add(artistaMusicaVO);
						
		}
		
		return listaConsultaArtistaMusicaDTO;
		
	}
	
	
	public List<TagArtistaMusica> listarTagArtistaMusica(Integer idArtistaMusica) {
		return tagArtistaMusicaRepository.listar(idArtistaMusica);
	}
	
	public void excluirTagArtistaMusica(Integer idTagArtistaMusica) {
		tagArtistaMusicaRepository.delete(tagArtistaMusicaRepository.consular(idTagArtistaMusica));
	}
	
	public void inserirTagArtistaMusica(Integer idArtistaMusica, Integer idTag) {
		TagDiscoArtistaMusica tagDiscoArtistaMusica = TagDiscoArtistaMusica.getByCodigo(idTag);
		if (tagDiscoArtistaMusica != null && !tagDiscoArtistaMusica.getListaComplemento().isEmpty()) {
			for (Integer idTagComplento : tagDiscoArtistaMusica.getListaComplemento()) {
				this.inserirTagArtistaMusica(idArtistaMusica, idTagComplento);
			}
		} else {
			TagArtistaMusica tagArtistaMusica = new TagArtistaMusica();
			tagArtistaMusica.setArtistaMusica(this.consultarArtistaMusica(idArtistaMusica));
			tagArtistaMusica.setTag(tagRepository.consular(idTag));
			tagArtistaMusicaRepository.save(tagArtistaMusica);
		}	
	}
	
	public void finalizar(Integer idAlbum) {
		List<ArtistaMusica> listaArtistaMusica = artistaMusicaRepository.listar(idAlbum);
		for (ArtistaMusica artistaMusica : listaArtistaMusica) {
			artistaMusica.setSituacao(6);
			entityManager.merge(artistaMusica);
		}
	}
	
	public void salvarMusica(Integer idArtistaMusica, String artista, String musica) {
		ArtistaMusica artistaMusica = this.consultarArtistaMusica(idArtistaMusica);
		artistaMusica.setArtista(artista);
		artistaMusica.setMusica(musica);
		entityManager.merge(artistaMusica);
	}

	public List<Album> listar() {
		return albumRepository.listar();
	}	
	
}
