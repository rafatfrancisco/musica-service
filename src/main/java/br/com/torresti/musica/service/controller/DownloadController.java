package br.com.torresti.musica.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.torresti.musica.service.service.DownloadService;
import br.com.torresti.musica.util.dto.CadastroDescksDTO;
import br.com.torresti.musica.util.dto.CadastroDownloadDTO;
import br.com.torresti.spring.to.RestTO;

@RestController
@RequestMapping(value = "/download-service")
public class DownloadController {

	@Autowired
	private DownloadService downloadService;

	@RequestMapping(value = "/cadastrar-decks", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<List<RestTO>> consultar(@RequestBody CadastroDescksDTO cadastroDescksDTO) {
		return new ResponseEntity<List<RestTO>>(downloadService.cadastrarDecks(cadastroDescksDTO), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cadastrar-download", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<List<RestTO>> cadastrarDownload(@RequestBody CadastroDownloadDTO cadastroDownloadDTO) {
		return new ResponseEntity<List<RestTO>>(downloadService.cadastrarDownload(cadastroDownloadDTO), HttpStatus.OK);
	}
	
}
