package br.com.torresti.musica.service.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.torresti.musica.crud.model.entidy.Tag;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long>{
	
	@Query("SELECT p FROM Tag p WHERE p.idTag = :idTag")
	public Tag consular(@Param("idTag") Integer idTag);
	
}
