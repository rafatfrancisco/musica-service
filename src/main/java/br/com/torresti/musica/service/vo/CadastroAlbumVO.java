package br.com.torresti.musica.service.vo;

import br.com.torresti.musica.service.util.MenuTela;
import br.com.torresti.spring.to.BaseTO;

public class CadastroAlbumVO extends BaseTO {

	private Integer tipoSite;

	private int flagConsulta;

	private String urlSite;
	
	public CadastroAlbumVO() {}
	
	public CadastroAlbumVO(MenuTela menuTela) {
		this.setTitulo(menuTela.getTitulo());
		this.setCaminho(menuTela.getCaminho());
	}

	public Integer getTipoSite() {
		return tipoSite;
	}

	public void setTipoSite(Integer tipoSite) {
		this.tipoSite = tipoSite;
	}

	public int getFlagConsulta() {
		return flagConsulta;
	}

	public void setFlagConsulta(int flagConsulta) {
		this.flagConsulta = flagConsulta;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

}
