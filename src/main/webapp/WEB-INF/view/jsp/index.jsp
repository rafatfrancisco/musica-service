<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Musica Service</title>
        <!-- VENDOR CSS -->
        <link href="<c:url value='/img/icone/music-service.ico'/>" rel="icon" type="image/x-icon">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/animate.css/animate.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bootgrid/jquery.bootgrid.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/google-material-color/dist/palette.css'/>" rel="stylesheet">
		<link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/farbtastic/farbtastic.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/chosen/chosen.min.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/summernote/dist/summernote.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css'/>" rel="stylesheet">	
		<!-- VENDOR CSS -->
        <!-- CSS -->
        <link href="<c:url value='../torresti-google-template/lib/css/app.min.1.css'/>" rel="stylesheet">
        <link href="<c:url value='../torresti-google-template/lib/css/app.min.2.css'/>" rel="stylesheet">
		<!-- CSS -->
        <!-- JAVASCRIPT LIBRARIES -->
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/jquery/dist/jquery.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/Waves/dist/waves.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bootstrap-growl/bootstrap-growl.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/moment/min/moment.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js'/>"></script>		
        <script src="<c:url value='../torresti-google-template/lib/vendors/bootgrid/jquery.bootgrid.updated.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js'/>"></script>
		<!-- JAVASCRIPT LIBRARIES -->        
        <!-- PLACEHOLDER FOR IE9 -->
        <!--[if IE 9 ]>
            <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js'/>"></script>
        <![endif]-->
		<!-- PLACEHOLDER FOR IE9 -->
        <script src="<c:url value='../torresti-google-template/lib/vendors/bower_components/chosen/chosen.jquery.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/fileinput/fileinput.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/input-mask/input-mask.min.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/vendors/farbtastic/farbtastic.min.js'/>"></script>
        <!-- PLACEHOLDER FOR IE9 -->		
		
		<!-- //TODO REMOVER JS -->
        <script src="<c:url value='../torresti-google-template/lib/js/functions.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/js/actions.js'/>"></script>
        <script src="<c:url value='../torresti-google-template/lib/js/demo.js'/>"></script>
        <!-- REMOVER -->        
        <!-- MUSICA SERVICE -->
	 	<script src="<c:url value='/js/geral/main.js'/>"></script>
	 	<!-- MUSICA SERVICE -->	
    </head>
    <body data-ma-header="blue">
        <header id="header" class="media">
			<!-- TITULO -->
            <div class="pull-left h-logo">
                <a href="index.html" class="hidden-xs">
                    <i class="zmdi zmdi-globe zmdi-hc-fw"></i>Musica Service
                    <small>área administrativa</small>
                </a>
                <div class="menu-collapse" data-ma-action="sidebar-open" data-ma-target="main-menu">
                    <div class="mc-wrap">
                        <div class="mcw-line top palette-White bg"></div>
                        <div class="mcw-line center palette-White bg"></div>
                        <div class="mcw-line bottom palette-White bg"></div>
                    </div>
                </div>
            </div>
			<!-- TITULO -->
			<!-- MENU USUARIO -->
            <ul class="pull-right h-menu">
                <li class="dropdown hidden-xs">
                    <a data-toggle="dropdown" href=""><i class="hm-icon zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>					
                        <li class="hidden-xs">
                            <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                        </li>
                        <li>
                            <a data-action="clear-localstorage" href=""><i class="zmdi zmdi-delete"></i> Clear Local Storage</a>
                        </li>
                        <li>
                            <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
                        </li>
                        <li>
                            <a href=""><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                <li class="hm-alerts" data-user-alert="sua-messages" data-ma-action="sidebar-open" data-ma-target="user-alerts">
                    <a href=""><i class="hm-icon zmdi zmdi-notifications"></i></a>
                </li>
                <li class="dropdown hm-profile">
                    <a data-toggle="dropdown" href="">
                        <img src="<c:url value='/img/profile/1.jpg'/>" alt="">
                    </a>
                </li>
            </ul>
			<!-- MENU USUARIO -->
        </header>
        <section id="main">
			<!--MENU LATERAL-->
            <aside id="s-main-menu" class="sidebar">
                <div class="smm-header">
                    <i class="zmdi zmdi-long-arrow-left" data-ma-action="sidebar-close"></i>
                </div>
                <ul class="smm-alerts">
                    <li data-user-alert="sua-messages" data-ma-action="sidebar-open" data-ma-target="user-alerts">
                        <i class="zmdi zmdi-email"></i>
                    </li>
                    <li data-user-alert="sua-notifications" data-ma-action="sidebar-open" data-ma-target="user-alerts">
                        <i class="zmdi zmdi-notifications"></i>
                    </li>
                </ul>
                <ul class="main-menu">
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-drink"></i> Download Album</a>
                        <ul>
                            <li><a onclick="acessarMenu('download-view/cadastro-album/carregar')">Cadastro Album </a></li>
                            <li><a onclick="acessarMenu('download-view/consulta-album/carregar')">Consulta Album </a></li>
                            <li><a onclick="acessarMenu('download-view/baixa-album/carregar')">Baixa Album </a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-drink"></i> Case Disco</a>
                        <ul>
                            <li><a onclick="acessarMenu('disco-view/case-disco/consulta/carregar')">Consulta Disco </a></li>
                            <li><a onclick="acessarMenu('disco-view/case-disco/organiza/carregar')">Organiza Disco </a></li>
                        </ul>
                    </li>
                    
                </ul>
            </aside>
			<!--MENU LATERAL-->
			<!--PAGE-->
            <section id="content">
                <div class="container">
					<script type="text/javascript">
						pageTransitionGet('${tela}', $('.container'));
					</script>            		
                </div>
            </section>
			<!--PAGE-->
			<!--FOOTER-->
            <footer id="footer">
                2018 &copy; Torres TI - www.torresti.com.br
            </footer>
			<!--FOOTER-->
        </section>
        <!-- PAGE LOADER -->
        <div class="page-loader palette-Blue bg">
            <div class="preloader pl-xl pls-white">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20"/>
                </svg>
            </div>
        </div>
		<!-- PAGE LOADER -->
    </body>
  </html>