<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frmDisco" modelAttribute="consultaDiscoVO">

<input type="hidden" class="mensagemSucesso" value="${consultaDiscoVO.mensagemSucesso}"/>
<input type="hidden" class="mensagemErro" value="${consultaDiscoVO.mensagemErro}"/>

<!--TITULO-->
<h4><i class="zmdi zmdi-drink zmdi-hc-fw"></i>${consultaDiscoVO.titulo}</h4>
<div class="c-header">
    <small>${consultaDiscoVO.caminho}</small>
</div>
<!--TITULO-->

<!--CORPO PAGINA-->
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Pesquisa Disco<small>Tela usada para consultar disco no sistema musica</small></h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Id</strong></p>
				<input type="text" class="form-control inp inp-id" name="id" value="${consultaDiscoVO.id}">
				<span class="help-block"> Insira o id </span>
			</div>
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Código</strong></p>
				<input type="text" class="form-control inp inp-codigo" name="codigo" value="${consultaDiscoVO.codigo}">
				<span class="help-block"> Insira o código </span>
			</div>			
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Selo</strong></p>
				<input type="text" class="form-control inp inp-selo" name="selo" value="${consultaDiscoVO.selo}">
				<span class="help-block"> Insira o selo </span>
			</div>
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Artista</strong></p>
				<input type="text" class="form-control inp inp-artista" name="artista" value="${consultaDiscoVO.artista}">
				<span class="help-block"> Insira o artista </span>
			</div>		
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Música</strong></p>
				<input type="text" class="form-control inp inp-musica" name="musica" value="${consultaDiscoVO.musica}">
				<span class="help-block"> Insira a música </span>
			</div>
			<div class="col-sm-2 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Album</strong></p>
				<input type="text" class="form-control inp inp-album" name="album" value="${consultaDiscoVO.album}">
				<span class="help-block"> Insira o album </span>
			</div>						
		</div>
		<div class="clearfix">
			<div class="pull-right">
				<input type="button" class="btn btn-primary waves-effect btn-consultar" value="Consultar Disco">
				<input type="button" class="btn btn-primary waves-effect btn-limpar" value="Limpar">
			</div>
		</div>
	</div>
</div>

<c:if test="${not empty consultaDiscoVO.listaDiscoDTO}">
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Lista Músicas Album</h2>
	</div>
	<div class="table-responsive">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th width="15%">Capa</th>
					<th width="40%">Album</th>
					<th width="45%">Configuração</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${consultaDiscoVO.listaDiscoDTO}" varStatus="status">
				<tr style="height: 100px;">
					<td>
						<c:if test="${not empty item.urlImagem}">
							<img src="${item.urlImagem}" alt="" height="120px" width="120px" onclick="consultarDisco(${item.idDisco});">
						</c:if>
						<c:if test="${empty item.urlImagem}">
							<img src="http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg" alt="" height="320px" width="320px" onclick="consultarDisco(${item.idDisco});">
						</c:if>							
						<input type="hidden" class="listaDiscoDTO[${status.index}].idDisco" name="listaDiscoDTO[${status.index}].idDisco" value="${item.idDisco}">
					</td>
					<td>
						<a href="${item.urlSite}" target="_blank">${item.idDisco} - ${item.dataCriacao} - ${item.nome} [${item.selo}]</a><br>
						<c:forEach var="musica" items="${item.listaMusicaDiscoDTO}">
							<small>${musica.artista} - ${musica.musica}</small><br/>
						</c:forEach>
					</td>
					<td>
						<div class="row">
							<div class="col-sm-6 m-b-20">
								<form:select class="form-control" path="listaDiscoDTO[${status.index}].situacao" disabled="true">
									<form:option value="0">Case</form:option>
									<form:option value="1">Perdeu</form:option>
									<form:option value="2">Vendeu</form:option>
								</form:select>							
							</div>
							<div class="col-sm-6 m-b-20">
								<form:select class="form-control" path="listaDiscoDTO[${status.index}].idPrateleira" disabled="true">
									<form:option value=""></form:option>
									<form:options items="${consultaDiscoVO.listaPrateleira}" itemValue="codigo" itemLabel="descricao"/>
								</form:select>							
							</div>
						</div>
					</td>						
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</c:if>

<c:if test="${not empty consultaDiscoVO.listaResultadoDiscoDTO}">
<c:forEach var="item" items="${consultaDiscoVO.listaResultadoDiscoDTO}" varStatus="status">
<br>
<div class="row">
	<c:if test="${not empty item.listaDiscoMenorDTO}">
	<div class="col-sm-4">
	    <div class="card z-depth-3">
	        <div class="card-header">
	            <h2>Discos Menor<small>Painel de discos abaixo do consultado</small></h2>
	        </div>
	        <div class="card-body card-padding">
				<div id="carousel-menor" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<c:forEach var="disco" items="${item.listaDiscoMenorDTO}" varStatus="status">
							<li data-target="#carousel-menor" data-slide-to="${status.index}" class="${disco.active}"></li>
						</c:forEach>
					</ol>
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<c:forEach var="disco" items="${item.listaDiscoMenorDTO}" varStatus="status">
							<div class="item ${disco.active}">
								<c:if test="${not empty disco.urlImagem}">
									<img src="${disco.urlImagem}" alt="" height="320px" width="320px">
								</c:if>
								<c:if test="${empty disco.urlImagem}">
									<img src="http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg" alt="" height="320px" width="320px">
								</c:if>								
								<div class="carousel-caption">
									<h3><a href="${disco.urlSite}" target="_blank">${disco.idDisco} - ${disco.nome}</a></h3>
									<p>${disco.selo}</p>
								</div>
							</div>						
						</c:forEach>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-menor" role="button" data-slide="prev">
						<span class="zmdi zmdi-chevron-left" aria-hidden="true"></span>
					</a>
					<a class="right carousel-control" href="#carousel-menor" role="button" data-slide="next">
						<span class="zmdi zmdi-chevron-right" aria-hidden="true"></span>
					</a>
				</div>
	        </div>
	    </div>
	</div>
	</c:if>
	<div class="col-sm-4">
	    <div class="card z-depth-3">
	        <div class="card-header">
	            <h2>Disco Encontrado<small>${item.nomePrateleira} - ${item.totalPrateleira} - Posicao ${item.posicaoPrateleira}</small></h2>
	        </div>
	        <div class="card-body card-padding">
				<div id="carousel-encontrado" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<c:if test="${not empty item.discoDTO.urlImagem}">
								<img src="${item.discoDTO.urlImagem}" alt="" height="320px" width="320px">
							</c:if>
							<c:if test="${empty item.discoDTO.urlImagem}">
								<img src="http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg" alt="" height="320px" width="320px">
							</c:if>							
							<div class="carousel-caption">
								<h3><a href="${item.discoDTO.urlSite}" target="_blank">${item.discoDTO.idDisco} - ${item.discoDTO.nome}</a></h3>
								<p>${item.discoDTO.selo}</p>
							</div>
						</div>
					</div>
				</div>
	        </div>
	    </div>
	</div>
	<c:if test="${not empty item.listaDiscoMaiorDTO}">
	<div class="col-sm-4">
	    <div class="card z-depth-3">
	        <div class="card-header">
	            <h2>Discos Maior<small>Painel de discos acima do consultado</small></h2>
	        </div>
	        <div class="card-body card-padding">
				<div id="carousel-maior" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<c:forEach var="disco" items="${item.listaDiscoMaiorDTO}" varStatus="status">
							<li data-target="#carousel-maior" data-slide-to="${status.index}" class="${disco.active}"></li>
						</c:forEach>
					</ol>
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<c:forEach var="disco" items="${item.listaDiscoMaiorDTO}" varStatus="status">
							<div class="item ${disco.active}">
								<c:if test="${not empty disco.urlImagem}">
									<img src="${disco.urlImagem}" alt="" height="320px" width="320px">
								</c:if>
								<c:if test="${empty disco.urlImagem}">
									<img src="http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg" alt="" height="320px" width="320px">
								</c:if>	
								<div class="carousel-caption">
									<h3><a href="${disco.urlSite}" target="_blank">${disco.idDisco} - ${disco.nome}</a></h3>
									<p>${disco.selo}</p>
								</div>
							</div>
						</c:forEach>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-maior" role="button" data-slide="prev">
						<span class="zmdi zmdi-chevron-left" aria-hidden="true"></span>
					</a>
					<a class="right carousel-control" href="#carousel-maior" role="button" data-slide="next">
						<span class="zmdi zmdi-chevron-right" aria-hidden="true"></span>
					</a>
				</div>
	        </div>
	    </div>
	</div>
	</c:if>
</div>
</c:forEach>
</c:if>
<!--CORPO PAGINA-->

</form:form>

<script src="<c:url value='/js/page/disco/consulta.js'/>"></script>