<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal fade" id="modal-altera-musica" tabindex="0" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Altera Música Disco</h4>
			</div>
			<div class="modal-body">
				<form:form id="frmMusicaDisco" commandName="alteraMusicaDiscoVO">
					<div class="row">
						<div class="col-sm-6 m-b-20">
							<p class="f-500 m-b-10 c-black"><strong>Arista</strong></p>
						</div>
						<div class="col-sm-6 m-b-20">
							<p class="f-500 m-b-10 c-black"><strong>Música</strong></p>		
						</div>					
					</div>
					<c:forEach var="item" items="${alteraMusicaDiscoVO.listaMusicaDiscoVO}" varStatus="status">
						<div class="row">
							<div class="col-sm-6 m-b-20">
								<form:hidden path="item.idMusicaDisco"/>
								<form:input class="form-control inp-artista" path="item.artista"/>
							</div>
							<div class="col-sm-6 m-b-20">
								<form:input class="form-control inp-artista" path="item.musica"/>			
							</div>					
						</div>										
					</c:forEach>
				</form:form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link waves-effect" onclick="salvarMusicas();">Salvar</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<!--<input type="text" class="form-control inp-musica" name="listaMusicaDiscoVO.musica" value=""/> -->