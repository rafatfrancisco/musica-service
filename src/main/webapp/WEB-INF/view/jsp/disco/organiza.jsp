<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frmDisco" modelAttribute="organizaDiscoVO">

<input type="hidden" class="mensagemSucesso" value="${organizaDiscoVO.mensagemSucesso}"/>
<input type="hidden" class="mensagemErro" value="${organizaDiscoVO.mensagemErro}"/>

<!--TITULO-->
<h4><i class="zmdi zmdi-drink zmdi-hc-fw"></i>${organizaDiscoVO.titulo}</h4>
<div class="c-header">
    <small>${organizaDiscoVO.caminho}</small>
</div>
<!--TITULO-->

<!--CORPO PAGINA-->
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Pesquisa Disco<small>Tela usada para consultar disco para ajuste no sistema musica</small></h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
			<div class="col-sm-3 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Código</strong></p>
				<input type="text" class="form-control inp-idDisco" name=idDisco value="${organizaDiscoVO.idDisco}">
				<span class="help-block"> Insira o código que quer consultar </span>
			</div>
			<div class="col-sm-3 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Pratileira</strong></p>
				<form:select class="form-control slc-prateleira" path="idPrateleira">
					<form:option value=""></form:option> 
					<form:options items="${organizaDiscoVO.listaPrateleira}" itemValue="codigo" itemLabel="descricao"/>
				</form:select>				
				<span class="help-block"> Configuração da pratileira </span>
			</div>
		</div>
        <hr>
		<div class="clearfix">
			<div class="pull-right">
				<input type="button" class="btn btn-primary waves-effect btn-consultar" value="Consultar">
				<input type="button" class="btn btn-primary waves-effect btn-proximo" value="Proxixo (+10)">
				<input type="button" class="btn btn-primary waves-effect btn-ajustar" value="Ajustar">
			</div>
		</div>		
	</div>
</div>
<c:if test="${not empty organizaDiscoVO.listaDiscoDTO}">
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Lista Músicas Album</h2>
	</div>
	<div class="table-responsive">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th width="15%">Capa</th>
					<th width="40%">Album</th>
					<th width="45%">Configuração</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${organizaDiscoVO.listaDiscoDTO}" varStatus="status">
				<tr style="height: 100px;">
					<td>
						<c:if test="${not empty item.urlImagem}">
							<img src="${item.urlImagem}" alt="" height="120px" width="120px">
						</c:if>
						<c:if test="${empty item.urlImagem}">
							<img src="http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg" alt="" height="120px" width="120px">
						</c:if>							
						<input type="hidden" class="listaDiscoDTO[${status.index}].idDisco" name="listaDiscoDTO[${status.index}].idDisco" value="${item.idDisco}">
					</td>
					<td>
						<a href="${item.urlSite}" target="_blank">${item.idDisco} - ${item.dataCriacao} - ${item.nome} [${item.selo}]</a><br>
						<c:forEach var="musica" items="${item.listaMusicaDiscoDTO}">
							<small>${musica.artista} - ${musica.musica}</small><br/>
						</c:forEach>
					</td>
					<td>
						<div class="row">
							<div class="col-sm-6 m-b-20">
								<form:select class="form-control" path="listaDiscoDTO[${status.index}].situacao">
									<form:option value="0">Case</form:option>
									<form:option value="1">Perdeu</form:option>
									<form:option value="2">Vendeu</form:option>
								</form:select>							
							</div>
							<div class="col-sm-6 m-b-20">
								<form:select class="form-control" path="listaDiscoDTO[${status.index}].idPrateleira">
									<form:option value=""></form:option>
									<form:options items="${organizaDiscoVO.listaPrateleira}" itemValue="codigo" itemLabel="descricao"/>
								</form:select>							
							</div>
							<div class="col-sm-12 m-b-20">
								<input type="button" class="btn btn-primary waves-effect" value="Alterar Musicas Album" onclick="alterarMusicaAlbum(${item.idDisco});">
							</div>
						</div>
						
					</td>						
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</c:if>
<!--CORPO PAGINA-->

</form:form>

<div class="div-altera-musica"></div>

<script src="<c:url value='/js/page/disco/organiza.js'/>"></script>