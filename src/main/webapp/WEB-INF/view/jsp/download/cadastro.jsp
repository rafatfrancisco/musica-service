<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frmCadastroAlbum" modelAttribute="cadastroAlbumVO">

<input type="hidden" class="mensagemSucesso" value="${cadastroAlbumVO.mensagemSucesso}"/>
<input type="hidden" class="mensagemErro" value="${cadastroAlbumVO.mensagemErro}"/>

<!--TITULO-->
<h4><i class="zmdi zmdi-drink zmdi-hc-fw"></i>${cadastroAlbumVO.titulo}</h4>
<div class="c-header">
    <small>${cadastroAlbumVO.caminho}</small>
</div>
<!--TITULO-->

<!--CORPO PAGINA-->
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Cadastrar Album<small>Tela usada para cadastrar album no sistema musica</small></h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
			<div class="col-sm-3 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Tipo Site</strong></p>
                <form:select class="form-control" path="tipoSite">
                	<form:option value="">Selecione</form:option>
                	<form:option value="1">Decks</form:option>
                	<form:option value="4">Juno</form:option>
                	<form:option value="5">DeejayDe</form:option>
                </form:select>
                <span class="help-block"> Insira a tipo do site que quer salvar </span>				
			</div>
			<div class="col-sm-3 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Consulta</strong></p>
                <form:select class="form-control flagConsulta" path="flagConsulta">
                	<form:option value="0">Não</form:option>
                	<form:option value="1">Sim</form:option>
                </form:select>
                <span class="help-block"> Insira a tipo do site que quer salvar </span>				
			</div>			
			<div class="col-sm-6 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Url Site</strong></p>
				<input type="text" class="form-control inp-urlSite" name="urlSite" value="${cadastroAlbumVO.urlSite}" placeholder="">
				<span class="help-block"> Insira a url do site que quer salvar </span>
			</div>
		</div>
		<br clear="all">
		<ul>
			<li>Decks: (https://www.decks.de/decks/workfloor/lists/findTrack.php?code=cgj-pl)</li>
			<li>Juno: (https://www.juno.co.uk/products/707887-01/)</li>
			<li>DeejayDe: (https://www.deejay.de/Kepler_Instinct_DTW001_DTW001_Vinyl__946277)</li>
		</ul> 		
		<div class="clearfix">
			<div class="pull-right">
				<input type="button" class="btn btn-primary waves-effect btn-salvar" value="Salvar Novo Album">
			</div>
		</div>
	</div>
</div>
<!--CORPO PAGINA-->

</form:form>

<script src="<c:url value='/js/page/download/cadastro.js'/>"></script>