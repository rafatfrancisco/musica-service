<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frmListaMusica" modelAttribute="listaMusicaVO">

<input type="hidden" class="mensagemSucesso" value="${listaMusicaVO.mensagemSucesso}"/>
<input type="hidden" class="mensagemErro" value="${listaMusicaVO.mensagemErro}"/>
<input type="hidden" class="idArtistaMusica" name="idArtistaMusica" value=""/>
<input type="hidden" class="situacaoMusica" name="situacaoMusica" value=""/>

<!--TITULO-->
<h4><i class="zmdi zmdi-drink zmdi-hc-fw"></i>${listaMusicaVO.titulo}</h4>
<div class="c-header">
    <small>${listaMusicaVO.caminho}</small>
</div>
<!--TITULO-->

<!--CORPO PAGINA-->
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Filtro Pesquisa<small>Usado para consultar a lista de download de música</small></h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
			<div class="col-sm-4 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Situação</strong></p>
				<form:select class="form-control" path="situacao">
					<form:option value="1">Baixar</form:option>
					<form:option value="2">Soulseek</form:option>
					<form:option value="3">Youtube</form:option>
					<form:option value="4">Comprar</form:option>
					<form:option value="5">Futuro</form:option>
				</form:select>
				<span class="help-block"> Selecione a situação da música </span>           		
			</div>
			<div class="col-sm-4 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Categoria</strong></p>
				<form:select class="form-control" path="categoria">
					<form:option value="">Todas</form:option>
					<form:option value="0">Normal</form:option>
					<form:option value="1">Legal</form:option>
					<form:option value="2">Genial</form:option>
					<form:option value="3">Fantastica</form:option>
					<form:option value="4">Foda</form:option>
					<form:option value="5">Urgente</form:option>
				</form:select>
				<span class="help-block"> Selecione a categoria da música </span>           		
			</div>
			<div class="col-sm-4 m-b-20">
				<p class="f-500 m-b-10 c-black"><strong>Tag</strong></p>
				<form:select class="form-control" path="idTag">
					<option value=""></option>
					<option value="20">Dub</option>
					<option value="21">Deep</option>
					<option value="22">House</option>
					<option value="23">Detroit</option>
					<option value="24">Minimal</option>
					<option value="25">Techno</option>
					<option value="26">Techouse</option>
					<option value="27">Break</option>
					<option value="28">Romeno</option>
					<option value="100">Acapella</option>
					<option value="101">Tools</option>
					<option value="102">Intro</option>
					<option value="103">Lounge</option>
					<option value="200">Villalobos</option>
					<option value="201">Raresh</option>
					<option value="202">Zip</option>
					<option value="203">Garnier</option>
					<option value="204">Vath</option>
				</form:select>
				<span class="help-block"> Selecione a tag da música </span>           		
			</div>
		</div>
		<div class="clearfix">
			<div class="pull-right">
				<input type="button" class="btn btn-primary waves-effect btn-consultar" value="Consultar Músicas">
			</div>
		</div>		
    </div>
</div>
<!--CORPO PAGINA-->

<c:if test="${not empty listaMusicaVO.listaArtistaMusicaVO}">
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Lista Música<small>Lista download de música</small></h2>
	</div>
	<div class="table-responsive">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th width="35%">Música</th>
					<th width="10%">Situação</th>
					<th width="10%">Categoria</th>
					<th width="15%">Tag</th>
					<th width="30%">Opções</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${listaMusicaVO.listaArtistaMusicaVO}" varStatus="status">
				<tr style="height: 100px;" class="item-lista-artista-musica-${item.idArtistaMusica}">
					<td>
						<a href="${item.link}" target="_blank">${item.musica}</a>
						<br/><span class="text-muted">${item.pesquisaMusica}</span>
					</td>
					<td>
						<form:select class="form-control select-situacao" path="listaArtistaMusicaVO[${status.index}].situacao" onchange="alterarSituacao(${item.idArtistaMusica}, this.value);">
							<form:option value="0">Cadastrada</form:option>
							<form:option value="1">Baixar</form:option>
							<form:option value="2">Soulseek</form:option>
							<form:option value="3">Youtube</form:option>
							<form:option value="4">Comprar</form:option>
							<form:option value="5">Futuro</form:option>
							<form:option value="6">Finalizada</form:option>
						</form:select>                     
					</td>                            
					<td>
						<form:select class="form-control" path="listaArtistaMusicaVO[${status.index}].categoria" disabled="true">
							<form:option value="0">Normal</form:option>
							<form:option value="1">Legal</form:option>
							<form:option value="2">Genial</form:option>
							<form:option value="3">Fantastica</form:option>
							<form:option value="4">Foda</form:option>
							<form:option value="5">Urgente</form:option>
						</form:select>
					</td>
					<td>
						<span class="text-muted">
							<c:forEach var="tag" items="${item.listaTagArtistaMusica}" varStatus="status01">
								<span class="tag label label-info">${tag.tag.nome}</span>
							</c:forEach>
						</span>
					</td>
					<td>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.beatport.com/search?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-headset"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.youtube.com/results?search_query=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-windows"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.discogs.com/search/?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-search-in-page"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.google.com.br/search?source=hp&q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-google"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://srv.muzbase.com/#/search?text=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-album"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://bandcamp.com/search?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-camera-alt"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" onclick="copiarTextoCrtlC('${item.pesquisaMusica}');" target="_blank"><i class="zmdi zmdi-border-color"></i></a>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</c:if>

</form:form>

<script src="<c:url value='/js/page/download/baixa.js'/>"></script>