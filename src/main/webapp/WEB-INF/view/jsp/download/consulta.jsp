<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="frmConsultaAlbum" modelAttribute="consultaAlbumVO">

<input type="hidden" class="mensagemSucesso" value="${consultaAlbumVO.mensagemSucesso}"/>
<input type="hidden" class="mensagemErro" value="${consultaAlbumVO.mensagemErro}"/>
<input type="hidden" class="idUltimoAlbum" name="idUltimoAlbum" value="${consultaAlbumVO.idUltimoAlbum}"/>
<input type="hidden" class="idAlbum" name="idAlbum" value="${consultaAlbumVO.idAlbum}"/>
<input type="hidden" class="idArtistaMusica" name="idArtistaMusica" value=""/>
<input type="hidden" class="idTagArtistaMusica" name="idTagArtistaMusica" value=""/>
<input type="hidden" class="idTag" name="idTag" value=""/>
<input type="hidden" class="categoria" name="categoria" value=""/>
<input type="hidden" class="situacao" name="situacao" value=""/>

<!--TITULO-->
<h4><i class="zmdi zmdi-drink zmdi-hc-fw"></i>${consultaAlbumVO.titulo}</h4>
<div class="c-header">
    <small>${consultaAlbumVO.caminho}</small>
</div>
<!--TITULO-->

<!--CORPO PAGINA-->
<div class="card z-depth-3">
	<div class="card-header">
		<h2>Dados Album<small>Tela usada para consultar e ajustar album no sistema musica</small></h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
		    <div class="col-md-12">
		        <div class="white-box">
		            <div class="row">
		            	<div class="col-md-2">
			                <p class="f-500 m-b-10 c-black"><strong>Id Album</strong></p>          		
		            	</div>
		            	<div class="col-md-4">
			                <p class="f-500 m-b-10 c-black"><strong>Tipo Site</strong></p>          		
		            	</div>
		                <div class="col-md-6">
		                    <p class="f-500 m-b-10 c-black"><strong>Url Site</strong></p>
		                </div>            
		            </div>
		            <div class="row">
		            	<div class="col-md-2">
		                    ${consultaAlbumVO.idAlbum}           		
		            	</div>
		            	<div class="col-md-4">
		                    ${consultaAlbumVO.tipoSite}           		
		            	</div>
		            	
		                <div class="col-md-6">
		                    <a href="${consultaAlbumVO.urlSite}" target="_blank">${consultaAlbumVO.urlSite}</a>
		                </div>            
		            </div>
		            <br/>
		            <div class="row">
		            	<div class="col-md-2">
			                <p class="f-500 m-b-10 c-black"><strong>Ano</strong></p>          		
		            	</div>
		            	<div class="col-md-4">
			                <p class="f-500 m-b-10 c-black"><strong>Nome</strong></p>          		
		            	</div>
		                <div class="col-md-6">
		                    <p class="f-500 m-b-10 c-black"><strong>Selo</strong></p>
		                </div>            
		            </div>
		            <div class="row">
		            	<div class="col-md-2">
			                ${consultaAlbumVO.ano}          		
		            	</div>
		            	<div class="col-md-4">
			                ${consultaAlbumVO.nome}          		
		            	</div>
		                <div class="col-md-6">
		                    ${consultaAlbumVO.selo}
		                </div>            
		            </div>
		            <hr>
					<div class="clearfix">
						<div class="pull-right">
							<input type="button" class="btn palette-Green bg waves-effect btn-finalizar" value="Finalizar">
							<input type="button" class="btn btn-primary waves-effect btn-primeiro-cadastrado" value="Primeiro Cadastrado">
                       		<input type="button" class="btn btn-primary waves-effect btn-anterior-010" value="Anterior -010">
                            <input type="button" class="btn btn-primary waves-effect btn-anterior" value="Anterior -001">
                           	<input type="button" class="btn btn-primary waves-effect btn-proximo" value="Próximo +001">
                           	<input type="button" class="btn btn-primary waves-effect btn-proximo-010" value="Próximos +010">
                           	<input type="button" class="btn btn-primary waves-effect btn-ultimo-cadastrado" value="Ultima Cadastrado">
						</div>
					</div>
		         </div>
		    </div>
		</div>
	</div>
</div>

<div class="card z-depth-3">
	<div class="card-header">
		<h2>Lista Músicas Album</h2>
	</div>
	<div class="table-responsive">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th width="20%">Música</th>
					<th width="10%">Situação</th>
					<th width="10%">Categoria</th>
					<th width="10%">Tag</th>
					<th width="20%"></th>
					<th width="30%">Opções</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${consultaAlbumVO.listaArtistaMusicaVO}" varStatus="status">
				<tr style="height: 100px;">
					<td>
						${item.musica}
						<br/><small>${item.pesquisaMusica}</small>
					</td>
					<td>
						<form:select class="form-control" path="listaArtistaMusicaVO[${status.index}].situacao" onchange="alterarSituacao(${item.idArtistaMusica}, this.value);">
							<form:option value="0">Cadastrada</form:option>
							<form:option value="1">Baixar</form:option>
							<form:option value="2">Soulseek</form:option>
							<form:option value="3">Youtube</form:option>
							<form:option value="4">Comprar</form:option>
							<form:option value="5">Futuro</form:option>
							<form:option value="6">Finalizada</form:option>
						</form:select>                          
					</td>                            
					<td>
						<form:select class="form-control" path="listaArtistaMusicaVO[${status.index}].categoria" onchange="alterarCategoria(${item.idArtistaMusica}, this.value);">
							<form:option value="0">Normal</form:option>
							<form:option value="1">Legal</form:option>
							<form:option value="2">Genial</form:option>
							<form:option value="3">Fantastica</form:option>
							<form:option value="4">Foda</form:option>
							<form:option value="5">Urgente</form:option>
						</form:select>
					</td>
					<td>
						<select class="form-control" onchange="inserirTagArtistaMusica(${item.idArtistaMusica}, this.value);">
							<option value=""></option>
							<c:forEach var="tag" items="${consultaAlbumVO.listaTag}" varStatus="status01">
								<option value="${tag.codigo}">${tag.descricao}</option>
							</c:forEach>
						</select>                            
					</td>
					<td>
						<span class="text-muted">
							<c:forEach var="tag" items="${item.listaTagArtistaMusica}" varStatus="status01">
								<span class="tag label label-info" onclick="excluirTag(${tag.idTagArtistaMusica});">${tag.tag.nome}</span>
							</c:forEach>
						</span>
					</td>
					<td>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.beatport.com/search?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-headset"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.youtube.com/results?search_query=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-windows"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.discogs.com/search/?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-search-in-page"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://www.google.com.br/search?source=hp&q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-google"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://srv.muzbase.com/#/search?text=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-album"></i></a>
						<a class="btn btn-primary btn-icon waves-effect waves-circle waves-float" href="https://bandcamp.com/search?q=${item.queryPesquisa}" target="_blank"><i class="zmdi zmdi-camera-alt"></i></a>
						<a class="btn btn-default btn-icon waves-effect waves-circle waves-float" onclick="editarMusica(${item.idArtistaMusica},'${item.artistaOriginal}','${item.musicaOriginal}');"><i class="zmdi zmdi-border-color"></i></a>                            
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		<br/>	
		<br/>
	</div>
</div>

<div class="modal fade" id="modalDefault" tabindex="0" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Música</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 m-b-20">
						<p class="f-500 m-b-10 c-black"><strong>Arista</strong></p>
						<input type="text" class="form-control inp-artista" name="artista" value=""/>
		                <span class="help-block"> Nome do Artista </span>				
					</div>
					<div class="col-sm-12 m-b-20">
						<p class="f-500 m-b-10 c-black"><strong>Música</strong></p>
						<input type="text" class="form-control inp-musica" name="musica" value=""/>
		                <span class="help-block"> Nome da música </span>				
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link waves-effect btn-salvar-musica">Salvar</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<!--CORPO PAGINA-->

</form:form>

<script src="<c:url value='/js/page/download/consulta.js'/>"></script>