$.ajaxSetup({
	cache : false
});

$(document).ajaxStart(function() {
	$('.page-loader').fadeIn();
}).ajaxStop(function() {
	setTimeout(function() {$('.page-loader').fadeOut(1000);}, 300);
}).ajaxComplete(function(event, request, settings) {
	setTimeout(function() {carregarMensagemTela();}, 900);
}).ajaxError(function(event, request, settings) {
	setTimeout(function() {apresentarMensagemErro(request.statusText);}, 900);
});

function getAjaxPadrao(url, sucessoCallback, erroCallback) {
	$.get(url,{},$.noop,"json").success(function(response) {
		tratarCallback(response, sucessoCallback, erroCallback);
	});	
}

function postAjaxPadrao(url, form, sucessoCallback, erroCallback) {
	$.post(url, serializeForm(form)).success(function(response) {
		tratarCallback(response, sucessoCallback, erroCallback);
	});	
}

function tratarCallback(response, sucessoCallback, erroCallback) {
	if (response.retorno == 0) {
		apresentarMensagemSucesso(response.mensagemSucesso);
		if (sucessoCallback) {
			sucessoCallback();
		}
	} else {
		apresentarMensagemErro(response.mensagemErro);
		if (erroCallback) {
			erroCallback();
		}			
	}	
}

function getPadraoSimples(url, form) {
	getPadrao(url, form, ".container", null);
}

function getPadrao(url, form, local, sucessoCallback) {
	$.get(url, serializeForm(form)).success(function(response) {
		getPostReturnPadrao(local, response, sucessoCallback);
	});	
}

function postPadraoSimples(url, form) {
	postPadrao(url, form, ".container", null);
}

function postPadrao(url, form, local, sucessoCallback) {
	$.post(url, serializeForm(form)).success(function(response) {
		getPostReturnPadrao(local, response, sucessoCallback);
	});	
}

function getPostReturnPadrao(local, response, sucessoCallback) {
	$(local).html(response);
	if (sucessoCallback) {
		sucessoCallback();
	}	
}

function getValor(find) {
	$(find).val();
}

function setValor(find, value) {
	$(find).val(value);
}

function desabilitarSelect(find) {
	$(find).prop('disabled', 'disabled');
}

function habilitarSelect() {
	//TODO
}

function carregarMensagemTela() {
	var mensagemSucesso = $('.mensagemSucesso').val();
	if (mensagemSucesso != null && mensagemSucesso != '') {
		apresentarMensagemSucesso(mensagemSucesso);
		$('.mensagemSucesso').val('');
	}
	var mensagemErro = $('.mensagemErro').val();
	if (mensagemErro != null && mensagemErro != '') {
		apresentarMensagemErro(mensagemErro);
		$('.mensagemErro').val('');
	}
}

function apresentarMensagemSucesso(texto) {
	if (texto != null && texto != '') {
		apresentarMensagem(' Mensagem de Sucesso! ', texto, 'success', 3500, 'animated flipInY', 'animated flipOutY');
	}
}

function apresentarMensagemErro(texto) {
	apresentarMensagem(' Mensagem de Erro! ', texto, 'danger', 0, 'animated flipInY', 'animated flipOutY');
}

function apresentarMensagem(title, message, type, timer, animateIn, animateOut) {
    $.growl({
        //icon: null,
        title: title,
        message: message,
        url: ''
    },{
        element: 'body',
        type: type,
        allow_dismiss: true,
        //placement: {from: "animated flipInY",align: "animated flipOutY"},
        offset: {
            x: 30,
            y: 30
        },
        spacing: 10,
        z_index: 1031,
        delay: 2500,
        timer: timer,
        url_target: '_blank',
        mouse_over: false,
        animate: {
        	enter: animateIn,
        	exit: animateOut
        },
        icon_type: 'class',
        template: '<div data-growl="container" class="alert" role="alert">' +
        '<button type="button" class="close" data-growl="dismiss">' +
        '<span aria-hidden="true">&times;</span>' +
        '<span class="sr-only">Close</span>' +
        '</button>' +
        '<span data-growl="icon"></span>' +
        '<span data-growl="title"></span>' +
        '<span data-growl="message"></span>' +
        '<a href="#" data-growl="url"></a>' +
        '</div>'
    });
}

function modalPadraoConfirma(titulo, mensagem, sucessoCallback, erroCallback) {
    swal({
        title: titulo,
        text: mensagem,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {        	
			if (sucessoCallback) {
				sucessoCallback();
			}
        } else {
			if (erroCallback) {
				erroCallback();
			}
        }
    });
}

function modalPadraoErro(titulo, mensagem) {
	swal(titulo, mensagem, "error");
}

function acessarMenu(opcao) {
	pageTransitionGet(opcao, $('.container'));
}

function pageTransitionGet(url, target, callback) {
	   $.get(url,{},$.noop,"html").success(function(response) {
			target.fadeOut(200, function() {
				target.html(response);
				target.fadeIn(800);
				if(callback){
					callback();
				}
			});
		}).error(function(response) {
			$('.container').html(response);
		});
	}

function pageTransitionPost(url, idForm, target, callback) {
	$.post(url, serializeForm(idForm)).success(function(response) {
		target.fadeOut(200, function() {
			target.html(response);
			target.fadeIn(800);
			if (callback) {
				callback();
			}
		});
	}).error(function(response) {
		$('.container').html(response);
	});
}

function serializeForm(idForm) {
	
	// Clona o form para remover as mascaras e submeter.
	var formSubmit = $('#' + idForm).clone();
		
	// Todos os campos do form Original
	var campos = $('#' + idForm).find(':input');

	// Todos os campos do form clonado
	var camposSubmit = formSubmit.find(':input');

	// Seleciona os campos que estão em divs ocultas.
	var camposDesabilitados = $('#' + idForm).find("div:hidden").find(":input");

	// Campos alterados dinamicamente (ajax) precisam ser copiados (o clone copia o valor original renderizado no HTML).
	for (var i = 0; i < campos.length; i++) {camposSubmit[i].value = campos[i].value;}

	// Remove as mascaras de moeda e percentual (mascaras de moeda, percentual, cpf e cnpj sao submetidas e removidas server-side);
	formSubmit.find(':input[type="text"]').filter('.moeda, .percentual, .cpf, .cnpj').each(function() {$(this).val($(this).maskMoney('unmasked')[0]);});
	
	return formSubmit.serialize();
	
}

function copiarTextoCrtlC(texto) {
	var textArea = document.createElement("textarea");
	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;
	textArea.style.width = '2em';
	textArea.style.height = '2em';
	textArea.style.padding = 0;
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';
	textArea.style.background = 'transparent';
	textArea.value = texto;
	document.body.appendChild(textArea);
	textArea.select();
	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Copying text command was ' + msg);
	} catch (err) {
		console.log('Oops, unable to copy');
		window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter",texto);
	}
	document.body.removeChild(textArea);
}

// -- ESPECIFICO -- //

function alterarMusicaAlbum(idDisco) {
	getPadrao('disco-view/case-disco/consulta/musicas/' + idDisco, abreModalMusicas, null);
}

function salvarMusicas() {
	postAjaxPadrao('disco-view/case-disco/salvar/musicas/ajax', 'frmMusicaDisco', fecharModal, null);
}

function abreModalMusicas() {
	$('#modal-altera-musica').show();
}

function fecharModalMusicas() {
	$('#modal-altera-musica').hide();
}