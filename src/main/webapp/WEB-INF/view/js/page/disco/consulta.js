$('.btn-consultar').on('click', function() {
	postPadraoSimples('disco-view/case-disco/consulta/consultar', 'frmDisco');
});

$('.btn-limpar').on('click', function() {
	this.setValor('.inp','');
});

function consultarDisco(idDisco) {
	this.setValor('.inp','');
	this.setValor('.inp-id',idDisco);
	postPadraoSimples('disco-view/case-disco/consulta/consultar', 'frmDisco');
}