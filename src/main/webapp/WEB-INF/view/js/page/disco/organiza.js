$('.btn-consultar').on('click', function() {
	postPadraoSimples('disco-view/case-disco/organiza/consultar', 'frmDisco');
});

$('.btn-proximo').on('click', function() {
	$('.inp-idDisco').val(10 + parseInt($('.inp-idDisco').val()));
	$('slc-prateleira').val('');
	postPadraoSimples('disco-view/case-disco/organiza/consultar', 'frmDisco');
});

$('.btn-ajustar').on('click', function() {
	postAjaxPadrao('disco-view/case-disco/organiza/ajustar/ajax', 'frmDisco', null, null);
});