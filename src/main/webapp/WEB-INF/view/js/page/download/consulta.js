$(function(){
	idAlbum = $('.idAlbum').val();
	idUltimoAlbum = $('.idUltimoAlbum').val();
	if (idAlbum == idUltimoAlbum) {
		$('.btn-proximo').hide();
		$('.btn-proximo-010').hide();
	}
});

$('.btn-anterior').on('click',function() {
	var idAlbum = Number($('.idAlbum').val()) - 1;
	carregar(idAlbum);
});

$('.btn-anterior-010').on('click',function() {
	var idAlbum = Number($('.idAlbum').val()) - 10;
	carregar(idAlbum);
});

$('.btn-proximo').on('click',function() {
	var idAlbum = Number($('.idAlbum').val()) + 1;
	carregar(idAlbum);
});

$('.btn-proximo-010').on('click',function() {
	var idAlbum = Number($('.idAlbum').val()) + 10;
	carregar(idAlbum);
});

$('.btn-primeiro-cadastrado').on('click',function() {
	var url = 'download-view/consulta-album/consultar/primeiro/cadastrado';
	$.get(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});
});

$('.btn-ultimo-cadastrado').on('click',function() {
	var url = 'download-view/consulta-album/consultar/ultimo/cadastrado';
	$.get(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});
});

$('.btn-finalizar').on('click',function() {
	var url = 'download-view/consulta-album/finalizar';
	$.post(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});
});

function carregar(idAlbum) {
	$('.idAlbum').val(idAlbum);
	recarregar();
}

function recarregar() {
	var url = 'download-view/consulta-album/consultar';
	$.post(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});	
}

function alterarSituacao(idArtistaMusica, situacao) {
	$('.idArtistaMusica').val(idArtistaMusica);
	$('.situacao').val(situacao);
	postAjaxPadrao('download-view/consulta-album/alterar/situacao/ajax', 'frmConsultaAlbum');
}

function alterarCategoria(idArtistaMusica, categoria) {
	$('.idArtistaMusica').val(idArtistaMusica);
	$('.categoria').val(categoria);
	postAjaxPadrao('download-view/consulta-album/alterar/categoria/ajax', 'frmConsultaAlbum');
}

function inserirTagArtistaMusica(idArtistaMusica, idTag) {
	$('.idArtistaMusica').val(idArtistaMusica);
	$('.idTag').val(idTag);
	if (idTag != '') {
		var url = 'download-view/consulta-album/inserir/tag';
		$.post(url, serializeForm('frmConsultaAlbum')).success(function(response) {
			$(".container").html(response);
		});		
	}
}

function excluirTag(idTagArtistaMusica) {
	$('.idTagArtistaMusica').val(idTagArtistaMusica);
	var url = 'download-view/consulta-album/excluir/tag';
	$.post(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});	
}

function excluirArtistaMusica(idArtistaMusica) {
	$('.idArtistaMusica').val(idArtistaMusica);
	var url = 'download-view/consulta-album/excluir/musica';
	$.post(url, serializeForm('frmConsultaAlbum')).success(function(response) {
		$(".container").html(response);
	});	
}

function editarMusica(idArtistaMusica, artista, musica) {
	$('.idArtistaMusica').val(idArtistaMusica);
	$('.inp-artista').val(artista);
	$('.inp-musica').val(musica);
	$("#modalDefault").modal('show');
}

$('.btn-salvar-musica').on('click',function() {
	postAjaxPadrao('download-view/consulta-album/salvar/musica/ajax', 'frmConsultaAlbum', carregarAposSalvar());
});

function carregarAposSalvar() {
	$("#modalDefault").modal('hide');
	setTimeout(function() {recarregar();}, 300);
}
