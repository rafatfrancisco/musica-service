$('.btn-consultar').on('click',function() {
	$('.select-situacao').prop('disabled', 'disabled');
	postPadraoSimples('download-view/baixa-album/consultar', 'frmListaMusica');	
});

function alterarSituacao(idArtistaMusica, situacaoMusica) {
	$('.select-situacao').prop('disabled', 'disabled');
	getAjaxPadrao('download-view/baixa-album/alterar/situacao/ajax/'+idArtistaMusica+'/'+situacaoMusica, atualizaLista(idArtistaMusica), null);
}

function atualizaLista(idArtistaMusica) {
	$('.item-lista-artista-musica-'+idArtistaMusica).remove();
	$('.select-situacao').removeAttr("disabled");	
}